package com.webapp.dat.authentication.data.constants;

public final class WebBusinessActionProperties {

    public static final String USER_ENTITY = "userEntity";
    public static final String USER = "user";
    public static final String LOGIN = "login";
    public static final String TOKEN = "token";

    private WebBusinessActionProperties() {
    }
}
