package com.webapp.dat.authentication.data.constants;

import com.webapp.dat.authentication.data.constants.abs.AWebBusinessActionNames;

public class PutActionNames extends AWebBusinessActionNames {

    public static final String UPDATE_USER = "UPDATE_USER";

    private PutActionNames() {
    }
}
