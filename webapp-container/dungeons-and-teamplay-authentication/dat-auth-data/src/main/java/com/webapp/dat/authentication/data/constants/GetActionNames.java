package com.webapp.dat.authentication.data.constants;

import com.webapp.dat.authentication.data.constants.abs.AWebBusinessActionNames;

public class GetActionNames extends AWebBusinessActionNames {

    public static final String GET_USER_BY_LOGIN = "GET_USER_BY_LOGIN";
    public static final String GET_USER_BY_TOKEN = "GET_USER_BY_TOKEN";
    public static final String RECREATE_USER_TOKEN = "RECREATE_USER_TOKEN";

    private GetActionNames() {
    }
}
