package com.webapp.dat.authentication.data.jpa.generator;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

public class ProjectIdentifierGenerator implements IdentifierGenerator {

    private static final String CALL_FUNCTION = "{? = call generate_primary_id()}";

    @Override
    public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
        try {
            Connection connection = session.connection();
            CallableStatement callback = connection.prepareCall(CALL_FUNCTION);
            callback.registerOutParameter(1, Types.BIGINT);
            callback.execute();
            Long result = callback.getLong(1);
            callback.close();
            return result;
        } catch (SQLException sqlException) {
            throw new HibernateException(sqlException);
        }
    }
}
