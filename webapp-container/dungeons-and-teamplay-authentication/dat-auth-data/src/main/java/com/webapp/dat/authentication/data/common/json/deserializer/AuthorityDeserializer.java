package com.webapp.dat.authentication.data.common.json.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.webapp.dat.authentication.data.common.utils.AuthorityDataUtils;
import org.springframework.security.core.GrantedAuthority;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

public class AuthorityDeserializer extends JsonDeserializer<Collection<GrantedAuthority>> {

    @Override
    public Collection<GrantedAuthority> deserialize(JsonParser jp, DeserializationContext context) throws IOException {
        Collection<GrantedAuthority> roles = new ArrayList<>();
        if (jp.getCurrentToken() == JsonToken.START_ARRAY) {
            while (jp.nextToken() != JsonToken.END_ARRAY) {
                roles.add(AuthorityDataUtils.getAuthority(jp.getValueAsString()));
            }
        }
        return roles;
    }
}
