package com.webapp.dat.authentication.data.role;

import com.webapp.dat.authentication.data.constants.UserRoles;
import com.webapp.dat.authentication.data.role.in.IAuthority;

public class AdminRole implements IAuthority {

    @Override
    public String getAuthority() {
        return UserRoles.ROLE_PREFIX.concat(UserRoles.ADMIN);
    }
}
