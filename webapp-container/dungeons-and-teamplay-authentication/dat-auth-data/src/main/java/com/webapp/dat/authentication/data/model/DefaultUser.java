package com.webapp.dat.authentication.data.model;

import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class DefaultUser extends User {

    public DefaultUser(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

    public DefaultUser(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
    }

    @Override
    public void eraseCredentials() {
        // we shouldn't clear password for default user
    }
}
