package com.webapp.dat.authentication.data.constants;

import com.webapp.dat.authentication.data.constants.abs.AWebBusinessActionNames;

public class PostActionNames extends AWebBusinessActionNames {

    public static final String CREATE_USER = "CREATE_USER";

    private PostActionNames() {
    }
}
