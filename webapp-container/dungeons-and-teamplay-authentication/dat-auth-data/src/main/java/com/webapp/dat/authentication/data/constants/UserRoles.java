package com.webapp.dat.authentication.data.constants;

public final class UserRoles {

    public static final String ROLE_PREFIX = "ROLE_";
    public static final String ROLE_DELIMITER = ", ";
    public static final String ADMIN = "ADMIN";
    public static final String SYSTEM = "SYSTEM";
    public static final String USER = "USER";

    private UserRoles() {

    }
}
