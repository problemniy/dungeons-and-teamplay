package com.webapp.dat.authentication.data.jpa.entity;

import com.webapp.dat.authentication.data.jpa.entity.root.ModelEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "dat_users")
public class UserEntity implements ModelEntity {

    @Id
    @GenericGenerator(name = "ProjectGenerator", strategy = "com.webapp.dat.authentication.data.jpa.generator.ProjectIdentifierGenerator")
    @GeneratedValue(generator = "ProjectGenerator")
    private Long id;

    @Column(name = "login", nullable = false, unique = true)
    private String login;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "access_id", referencedColumnName = "id")
    private AccessEntity access;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "info_id", referencedColumnName = "id")
    private InfoEntity info;

    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = false)
    private TokenEntity token;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public AccessEntity getAccess() {
        return access;
    }

    public void setAccess(AccessEntity access) {
        this.access = access;
    }

    public InfoEntity getInfo() {
        return info;
    }

    public void setInfo(InfoEntity info) {
        this.info = info;
    }

    public TokenEntity getToken() {
        return token;
    }

    public void setToken(TokenEntity token) {
        this.token = token;
    }
}
