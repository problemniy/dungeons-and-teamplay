package com.webapp.dat.authentication.data.role.in;

import org.springframework.security.core.GrantedAuthority;

public interface IAuthority extends GrantedAuthority {

}
