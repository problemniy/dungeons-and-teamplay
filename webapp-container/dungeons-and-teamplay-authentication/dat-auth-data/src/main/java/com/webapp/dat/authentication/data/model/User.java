package com.webapp.dat.authentication.data.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

@JsonIgnoreProperties({"accountNonExpired", "accountNonLocked", "authorities", "credentialsNonExpired", "enabled"})
public class User extends org.springframework.security.core.userdetails.User {

    private final AccountData accountData;
    private final VerificationToken verificationToken = new VerificationToken();
    //access data should be used by json only
    private final AccessData accessData;

    @JsonCreator
    public User(@JsonProperty("login") String username, @JsonProperty("password") String password,
                @JsonProperty("accountData") AccountData accountData, @JsonProperty("accessData") AccessData accessData) {
        super(username, password, accessData.isEnabled(), accessData.isAccountNonExpired(),
                accessData.isCredentialsNonExpired(), accessData.isAccountNonLocked(), accessData.getAuthorities());
        this.accountData = accountData;
        this.accessData = accessData;
    }

    public User(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
        accountData = new AccountData();
        accessData = createAccessDataInternally();
    }

    public User(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
        accountData = new AccountData();
        accessData = createAccessDataInternally();
    }

    public AccountData getAccountData() {
        return accountData;
    }

    public AccessData getAccessData() {
        return accessData;
    }

    private AccessData createAccessDataInternally() {
        AccessData accessData = new AccessData();
        accessData.setAccountNonExpired(isCredentialsNonExpired());
        accessData.setAccountNonLocked(isAccountNonLocked());
        accessData.setCredentialsNonExpired(isCredentialsNonExpired());
        accessData.setEnabled(isEnabled());
        accessData.setAuthorities(getAuthorities());
        return accessData;
    }

    @JsonGetter("login")
    @Override
    public String getUsername() {
        return super.getUsername();
    }

    @JsonGetter("token")
    public VerificationToken getVerificationToken() {
        return verificationToken;
    }
}
