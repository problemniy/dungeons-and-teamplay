package com.webapp.dat.authentication.data.jpa.entity;

import com.webapp.dat.authentication.data.jpa.entity.root.ModelEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "dat_access")
public class AccessEntity implements ModelEntity {

    @Id
    @GenericGenerator(name = "ProjectGenerator", strategy = "com.webapp.dat.authentication.data.jpa.generator.ProjectIdentifierGenerator")
    @GeneratedValue(generator = "ProjectGenerator")
    private Long id;

    @Column(name = "hash")
    private String hash;

    @OneToOne(mappedBy = "access", cascade = CascadeType.ALL, fetch = FetchType.EAGER, optional = false)
    private UserEntity user;

    @Column(name = "roles", nullable = false)
    private String roles;

    @Column(name = "enabled", nullable = false)
    private boolean enabled;

    @Column(name = "banned", nullable = false)
    private boolean banned;

    @Column(name = "active_to")
    private LocalDate activeTo;

    @Column(name = "pass_active_to")
    private LocalDate passwordActiveTo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isBanned() {
        return banned;
    }

    public void setBanned(boolean banned) {
        this.banned = banned;
    }

    public LocalDate getActiveTo() {
        return activeTo;
    }

    public void setActiveTo(LocalDate activeTo) {
        this.activeTo = activeTo;
    }

    public LocalDate getPasswordActiveTo() {
        return passwordActiveTo;
    }

    public void setPasswordActiveTo(LocalDate passwordActiveTo) {
        this.passwordActiveTo = passwordActiveTo;
    }
}
