package com.webapp.dat.authentication.data.common.utils;

import com.webapp.dat.authentication.data.constants.UserRoles;
import com.webapp.dat.authentication.data.role.AdminRole;
import com.webapp.dat.authentication.data.role.SystemRole;
import com.webapp.dat.authentication.data.role.UserRole;
import com.webapp.dat.authentication.data.role.in.IAuthority;
import org.springframework.lang.Nullable;
import org.springframework.security.core.GrantedAuthority;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

public final class AuthorityDataUtils {

    private AuthorityDataUtils() {
    }

    public static List<GrantedAuthority> parseRoles(List<String> roles) {
        return parseRoles(roles.toArray(String[]::new));
    }

    public static List<GrantedAuthority> parseAccessLine(String access) {
        return parseRoles(access.split(UserRoles.ROLE_DELIMITER));
    }

    public static List<GrantedAuthority> parseRoles(String[] roles) {
        return Arrays.stream(roles).map(AuthorityDataUtils::getAuthority).filter(Objects::nonNull).collect(Collectors.toList());
    }

    @Nullable
    public static IAuthority getAuthority(String authorityName) {
        IAuthority result = null;
        String role = authorityName.toUpperCase();
        switch (role) {
            case UserRoles.ADMIN:
                result = new AdminRole();
                break;
            case UserRoles.SYSTEM:
                result = new SystemRole();
                break;
            case UserRoles.USER:
                result = new UserRole();
                break;
        }
        return result;
    }

    public static String asSpringRole(String roleName) {
        String result = roleName;
        if (!result.startsWith(UserRoles.ROLE_PREFIX)) {
            result = UserRoles.ROLE_PREFIX.concat(result);
        }
        return result;
    }

    public static String asRole(String roleName) {
        return roleName.replace(UserRoles.ROLE_PREFIX, "");
    }

    public static String[] asStringRolesArray(Collection<? extends GrantedAuthority> roles, boolean asSpringRole) {
        return roles.stream().map(
                (Function<GrantedAuthority, String>) grantedAuthority -> {
                    String role = grantedAuthority.getAuthority();
                    return asSpringRole ? asSpringRole(role) : asRole(role);
                }
        ).toArray(String[]::new);
    }

    public static String createAccessLine(String... roles) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < roles.length; i++) {
            stringBuilder.append(roles[i].replace(UserRoles.ROLE_PREFIX, ""));
            if (i != roles.length - 1) {
                stringBuilder.append(", ");
            }
        }
        return stringBuilder.toString();
    }

    public static String createAccessLine(Collection<GrantedAuthority> roles) {
        return createAccessLine(roles.stream().map(GrantedAuthority::getAuthority).toArray(String[]::new));
    }
}
