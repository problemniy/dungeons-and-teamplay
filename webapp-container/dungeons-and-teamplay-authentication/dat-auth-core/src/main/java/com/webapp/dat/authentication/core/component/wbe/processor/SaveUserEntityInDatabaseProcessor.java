package com.webapp.dat.authentication.core.component.wbe.processor;

import com.tools.wbe.data.business.delegate.IWebBusinessProcess;
import com.tools.wbe.data.business.processor.IPostActionProcessor;
import com.tools.wbe.data.business.processor.IPutActionProcessor;
import com.tools.wbe.data.business.wrapper.WebBusinessProperties;
import com.tools.wbe.data.context.PostRequestContext;
import com.tools.wbe.data.context.PostResponseContext;
import com.tools.wbe.data.context.PutRequestContext;
import com.tools.wbe.data.context.PutResponseContext;
import com.webapp.dat.authentication.data.constants.WebBusinessActionProperties;
import com.webapp.dat.authentication.data.jpa.entity.UserEntity;
import com.webapp.dat.authentication.manage.component.repository.IUsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("saveUserEntityInDatabaseProcessor")
public class SaveUserEntityInDatabaseProcessor implements IPostActionProcessor, IPutActionProcessor {

    private final IUsersRepository usersRepository;

    @Autowired
    public SaveUserEntityInDatabaseProcessor(IUsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @Override
    public void execute(PostRequestContext requestContext, PostResponseContext responseContext, IWebBusinessProcess businessProcess) {
        saveUser(businessProcess.properties());
    }

    @Override
    public void execute(PutRequestContext requestContext, PutResponseContext responseContext, IWebBusinessProcess businessProcess) {
        saveUser(businessProcess.properties());
    }

    private void saveUser(WebBusinessProperties wbeProperties) {
        UserEntity user = wbeProperties.getProperty(WebBusinessActionProperties.USER_ENTITY, UserEntity.class);
        if (user != null) {
            wbeProperties.addProperty(WebBusinessActionProperties.USER_ENTITY, usersRepository.saveAndFlush(user));
        }
    }
}
