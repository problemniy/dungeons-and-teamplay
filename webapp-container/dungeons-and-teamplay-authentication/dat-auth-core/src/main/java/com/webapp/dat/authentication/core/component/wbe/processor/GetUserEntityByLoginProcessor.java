package com.webapp.dat.authentication.core.component.wbe.processor;

import com.tools.wbe.data.business.delegate.IWebBusinessProcess;
import com.tools.wbe.data.business.processor.IGetActionProcessor;
import com.tools.wbe.data.context.GetRequestContext;
import com.tools.wbe.data.context.GetResponseContext;
import com.webapp.dat.authentication.data.constants.WebBusinessActionProperties;
import com.webapp.dat.authentication.data.jpa.entity.UserEntity;
import com.webapp.dat.authentication.manage.component.repository.IUsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("getUserEntityByLoginProcessor")
public class GetUserEntityByLoginProcessor implements IGetActionProcessor {

    private final IUsersRepository usersRepository;

    @Autowired
    public GetUserEntityByLoginProcessor(IUsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @Override
    public void execute(GetRequestContext requestContext, GetResponseContext responseContext, IWebBusinessProcess businessProcess) {
        String login = businessProcess.properties().getProperty(WebBusinessActionProperties.LOGIN, String.class);
        if (login == null) {
            return;
        }
        UserEntity user = usersRepository.findByLogin(login);
        businessProcess.properties().addProperty(WebBusinessActionProperties.USER_ENTITY, user);
    }
}
