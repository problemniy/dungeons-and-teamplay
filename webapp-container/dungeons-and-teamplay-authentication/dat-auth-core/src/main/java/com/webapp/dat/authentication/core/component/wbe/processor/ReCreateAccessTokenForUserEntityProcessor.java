package com.webapp.dat.authentication.core.component.wbe.processor;

import com.tools.wbe.data.business.delegate.IWebBusinessProcess;
import com.tools.wbe.data.business.processor.IPostActionProcessor;
import com.tools.wbe.data.business.processor.IPutActionProcessor;
import com.tools.wbe.data.context.PostRequestContext;
import com.tools.wbe.data.context.PostResponseContext;
import com.tools.wbe.data.context.PutRequestContext;
import com.tools.wbe.data.context.PutResponseContext;
import com.webapp.dat.authentication.data.constants.WebBusinessActionProperties;
import com.webapp.dat.authentication.data.jpa.entity.TokenEntity;
import com.webapp.dat.authentication.data.jpa.entity.UserEntity;
import com.webapp.dat.authentication.manage.component.service.ISecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component("reCreateAccessTokenForUserEntityProcessor")
public class ReCreateAccessTokenForUserEntityProcessor implements IPostActionProcessor, IPutActionProcessor {

    private final ISecurityService securityService;

    @Autowired
    public ReCreateAccessTokenForUserEntityProcessor(ISecurityService securityService) {
        this.securityService = securityService;
    }

    @Override
    public void execute(PutRequestContext requestContext, PutResponseContext responseContext, IWebBusinessProcess businessProcess) {
        UserEntity userEntity = businessProcess.properties().getProperty(WebBusinessActionProperties.USER_ENTITY, UserEntity.class);
        if (userEntity == null) {
            return;
        }
        execute(userEntity);
    }

    @Override
    public void execute(PostRequestContext requestContext, PostResponseContext responseContext, IWebBusinessProcess businessProcess) {
        UserEntity userEntity = businessProcess.properties().getProperty(WebBusinessActionProperties.USER_ENTITY, UserEntity.class);
        if (userEntity == null) {
            return;
        }
        execute(userEntity);
    }

    private void execute(UserEntity userEntity) {
        String token = generateToken();
        TokenEntity tokenEntity = securityService.createTokenEntity(userEntity, token);
        if (userEntity.getToken() != null) {
            // just update token
            userEntity.getToken().setExpiryDate(tokenEntity.getExpiryDate());
            userEntity.getToken().setValue(tokenEntity.getValue());
        } else userEntity.setToken(tokenEntity);
    }

    private String generateToken() {
        return UUID.randomUUID().toString();
    }
}
