package com.webapp.dat.authentication.core.component.wbe.processor;

import com.tools.wbe.data.business.delegate.IWebBusinessProcess;
import com.tools.wbe.data.business.processor.IGetActionProcessor;
import com.tools.wbe.data.business.processor.IPostActionProcessor;
import com.tools.wbe.data.business.processor.IPutActionProcessor;
import com.tools.wbe.data.business.wrapper.WebBusinessProperties;
import com.tools.wbe.data.context.GetRequestContext;
import com.tools.wbe.data.context.GetResponseContext;
import com.tools.wbe.data.context.PostRequestContext;
import com.tools.wbe.data.context.PostResponseContext;
import com.tools.wbe.data.context.PutRequestContext;
import com.tools.wbe.data.context.PutResponseContext;
import com.webapp.dat.authentication.data.constants.WebBusinessActionProperties;
import com.webapp.dat.authentication.data.jpa.entity.UserEntity;
import com.webapp.dat.authentication.manage.component.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("createUserByUserEntityProcessor")
public class CreateUserByUserEntityProcessor implements IGetActionProcessor, IPostActionProcessor, IPutActionProcessor {

    private final UserService userService;

    @Autowired
    public CreateUserByUserEntityProcessor(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void execute(GetRequestContext requestContext, GetResponseContext responseContext, IWebBusinessProcess businessProcess) {
        fillUser(businessProcess.properties());
    }

    @Override
    public void execute(PostRequestContext requestContext, PostResponseContext responseContext, IWebBusinessProcess businessProcess) {
        fillUser(businessProcess.properties());
    }

    @Override
    public void execute(PutRequestContext requestContext, PutResponseContext responseContext, IWebBusinessProcess businessProcess) {
        fillUser(businessProcess.properties());
    }

    private void fillUser(WebBusinessProperties wbeProperties) {
        UserEntity userEntity = wbeProperties.getProperty(WebBusinessActionProperties.USER_ENTITY, UserEntity.class);
        if (userEntity != null) {
            wbeProperties.addProperty(WebBusinessActionProperties.USER, userService.convertToUser(userEntity));
        }
    }
}
