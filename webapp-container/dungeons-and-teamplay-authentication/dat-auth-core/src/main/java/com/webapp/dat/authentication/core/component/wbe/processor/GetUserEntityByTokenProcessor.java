package com.webapp.dat.authentication.core.component.wbe.processor;

import com.tools.wbe.data.business.delegate.IWebBusinessProcess;
import com.tools.wbe.data.business.processor.IGetActionProcessor;
import com.tools.wbe.data.business.processor.IPutActionProcessor;
import com.tools.wbe.data.context.GetRequestContext;
import com.tools.wbe.data.context.GetResponseContext;
import com.tools.wbe.data.context.PutRequestContext;
import com.tools.wbe.data.context.PutResponseContext;
import com.webapp.dat.authentication.data.constants.WebBusinessActionProperties;
import com.webapp.dat.authentication.data.jpa.entity.UserEntity;
import com.webapp.dat.authentication.manage.component.repository.IUsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("getUserEntityByTokenProcessor")
public class GetUserEntityByTokenProcessor implements IGetActionProcessor, IPutActionProcessor {

    private final IUsersRepository usersRepository;

    @Autowired
    public GetUserEntityByTokenProcessor(IUsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @Override
    public void execute(GetRequestContext requestContext, GetResponseContext responseContext, IWebBusinessProcess businessProcess) {
        commonExecute(businessProcess);
    }

    @Override
    public void execute(PutRequestContext requestContext, PutResponseContext responseContext, IWebBusinessProcess businessProcess) {
        commonExecute(businessProcess);
    }

    private void commonExecute(IWebBusinessProcess businessProcess) {
        String token = businessProcess.properties().getProperty(WebBusinessActionProperties.TOKEN, String.class);
        if (token == null) {
            return;
        }
        UserEntity user = usersRepository.findByToken(token);
        businessProcess.properties().addProperty(WebBusinessActionProperties.USER_ENTITY, user);
    }
}
