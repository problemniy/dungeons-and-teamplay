package com.webapp.dat.authentication.core.component.wbe.processor.endpoint;

import com.tools.wbe.data.business.delegate.IWebBusinessProcess;
import com.tools.wbe.data.business.processor.IGetActionProcessor;
import com.tools.wbe.data.business.processor.IPostActionProcessor;
import com.tools.wbe.data.business.processor.IPutActionProcessor;
import com.tools.wbe.data.business.wrapper.WebBusinessProperties;
import com.tools.wbe.data.context.GetRequestContext;
import com.tools.wbe.data.context.GetResponseContext;
import com.tools.wbe.data.context.PostRequestContext;
import com.tools.wbe.data.context.PostResponseContext;
import com.tools.wbe.data.context.PutRequestContext;
import com.tools.wbe.data.context.PutResponseContext;
import com.tools.wbe.data.context.type.IWebBodyContext;
import com.webapp.dat.authentication.data.constants.WebBusinessActionProperties;
import com.webapp.dat.authentication.data.model.User;
import org.springframework.stereotype.Component;

@Component("setBodyAsUserProcessor")
public class SetBodyAsUserProcessor implements IGetActionProcessor, IPostActionProcessor, IPutActionProcessor {

    @Override
    public void execute(GetRequestContext requestContext, GetResponseContext responseContext, IWebBusinessProcess businessProcess) {
        fillUser(responseContext, businessProcess.properties());
    }

    @Override
    public void execute(PostRequestContext requestContext, PostResponseContext responseContext, IWebBusinessProcess businessProcess) {
        fillUser(responseContext, businessProcess.properties());
    }

    @Override
    public void execute(PutRequestContext requestContext, PutResponseContext responseContext, IWebBusinessProcess businessProcess) {
        fillUser(responseContext, businessProcess.properties());
    }

    private void fillUser(IWebBodyContext responseContext, WebBusinessProperties wbeProperties) {
        User user = wbeProperties.getProperty(WebBusinessActionProperties.USER, User.class);
        if (user != null) {
            responseContext.setBody(user);
        }
    }
}
