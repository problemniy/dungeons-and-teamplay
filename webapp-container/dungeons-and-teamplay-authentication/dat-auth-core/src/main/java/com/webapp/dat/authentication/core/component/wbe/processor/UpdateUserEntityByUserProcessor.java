package com.webapp.dat.authentication.core.component.wbe.processor;

import com.tools.wbe.data.business.delegate.IWebBusinessProcess;
import com.tools.wbe.data.business.processor.IPutActionProcessor;
import com.tools.wbe.data.context.PutRequestContext;
import com.tools.wbe.data.context.PutResponseContext;
import com.webapp.dat.authentication.data.constants.WebBusinessActionProperties;
import com.webapp.dat.authentication.data.jpa.entity.UserEntity;
import com.webapp.dat.authentication.data.model.User;
import com.webapp.dat.authentication.manage.component.repository.IUsersRepository;
import com.webapp.dat.authentication.manage.component.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component("updateUserEntityByUserProcessor")
public class UpdateUserEntityByUserProcessor implements IPutActionProcessor {

    private final IUserService userService;
    private final IUsersRepository usersRepository;

    @Autowired
    public UpdateUserEntityByUserProcessor(IUserService userService, IUsersRepository usersRepository) {
        this.userService = userService;
        this.usersRepository = usersRepository;
    }

    @Override
    public void execute(PutRequestContext requestContext, PutResponseContext responseContext, IWebBusinessProcess businessProcess) {
        User user = requestContext.getBody(User.class);
        if (user == null) {
            return;
        }
        UserEntity preUserEntity = usersRepository.findByLogin(user.getUsername());
        Assert.notNull(preUserEntity, "User with login '" + user.getUsername() + "' doesn't exist");
        // update user entity in properties
        userService.updateUserEntity(preUserEntity, user);
        businessProcess.properties().addProperty(WebBusinessActionProperties.USER_ENTITY, preUserEntity);
    }
}
