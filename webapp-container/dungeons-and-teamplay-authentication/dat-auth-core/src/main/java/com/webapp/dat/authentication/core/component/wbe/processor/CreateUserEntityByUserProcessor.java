package com.webapp.dat.authentication.core.component.wbe.processor;

import com.tools.wbe.data.business.delegate.IWebBusinessProcess;
import com.tools.wbe.data.business.processor.IPostActionProcessor;
import com.tools.wbe.data.context.PostRequestContext;
import com.tools.wbe.data.context.PostResponseContext;
import com.webapp.dat.authentication.data.constants.UserRoles;
import com.webapp.dat.authentication.data.constants.WebBusinessActionProperties;
import com.webapp.dat.authentication.data.jpa.entity.UserEntity;
import com.webapp.dat.authentication.data.model.User;
import com.webapp.dat.authentication.manage.component.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("createUserEntityByUserProcessor")
public class CreateUserEntityByUserProcessor implements IPostActionProcessor {

    private final UserService userService;

    @Autowired
    public CreateUserEntityByUserProcessor(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void execute(PostRequestContext requestContext, PostResponseContext responseContext, IWebBusinessProcess businessProcess) {
        User user = requestContext.getBody(User.class);
        if (user == null) {
            return;
        }
        UserEntity userEntity = userService.convertToUserEntity(user);
        userEntity.getAccess().setRoles(UserRoles.USER);
        businessProcess.properties().addProperty(WebBusinessActionProperties.USER_ENTITY, userEntity);
    }
}
