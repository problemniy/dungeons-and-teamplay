package com.webapp.dat.authentication.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(
        exclude = {
                org.springframework.boot.autoconfigure.gson.GsonAutoConfiguration.class
        })
@EnableEurekaClient
@ComponentScan({
        "com.webapp.dat.authentication.core",
        "com.webapp.dat.authentication.manage"
})
@EntityScan("com.webapp.dat.authentication.data.jpa.entity")
@ImportResource("classpath:/context/wbe-processors.config.xml")
@EnableJpaRepositories(basePackages = "com.webapp.dat.authentication.manage.component.repository")
@PropertySources({
        @PropertySource("classpath:security.properties"),
        @PropertySource("classpath:client.properties"),
        @PropertySource("classpath:database.properties")
})
public class DATAuthenticationApp {
    public static void main(String[] args) {
        SpringApplication.run(DATAuthenticationApp.class, args);
    }
}
