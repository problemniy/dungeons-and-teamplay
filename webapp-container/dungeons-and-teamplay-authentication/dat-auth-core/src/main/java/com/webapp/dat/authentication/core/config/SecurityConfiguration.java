package com.webapp.dat.authentication.core.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(SecurityConfiguration.class)
@ConfigurationProperties(prefix = "security")
public class SecurityConfiguration {

    private String eurekaGatePort;

    public String getEurekaGatePort() {
        return eurekaGatePort;
    }

    public void setEurekaGatePort(String eurekaGatePort) {
        this.eurekaGatePort = eurekaGatePort;
    }

}
