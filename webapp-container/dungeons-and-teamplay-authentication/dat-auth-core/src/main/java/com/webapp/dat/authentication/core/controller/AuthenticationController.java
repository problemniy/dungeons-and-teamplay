package com.webapp.dat.authentication.core.controller;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/auth")
public class AuthenticationController {

    @GetMapping(value = "/wbe/mapping", produces = MediaType.TEXT_HTML_VALUE)
    public String wbeMappingDelegate() {
        return "forward:/wbe/mapping";
    }
}
