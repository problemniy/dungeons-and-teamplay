package com.webapp.dat.authentication.core.controller.rest;

import com.webapp.dat.authentication.data.model.User;
import com.webapp.dat.authentication.manage.component.service.IBusinessManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/auth")
public class AuthenticationRestController {

    private final IBusinessManagementService businessManagementService;

    @Autowired
    public AuthenticationRestController(IBusinessManagementService businessManagementService) {
        this.businessManagementService = businessManagementService;
    }

    @PostMapping(path = "/createUser", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> createUser(@RequestBody User user, @RequestHeader HttpHeaders headers, HttpServletRequest request, HttpServletResponse response) {
        return businessManagementService.createUser(headers, request, response, user);
    }

    @RequestMapping(path = "/updateUser", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> updateUser(@RequestBody User user, @RequestHeader HttpHeaders headers, HttpServletRequest request, HttpServletResponse response) {
        return businessManagementService.updateUser(headers, request, response, user);
    }

    @GetMapping(value = "/getUserByName", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> getUserByName(@RequestParam String name, @RequestHeader HttpHeaders headers, HttpServletRequest request, HttpServletResponse response) {
        return businessManagementService.getUserByLogin(headers, request, response, name);
    }

    @GetMapping(value = "/getUserByToken", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> getUserByToken(@RequestParam String token, @RequestHeader HttpHeaders headers, HttpServletRequest request, HttpServletResponse response) {
        return businessManagementService.getUserByToken(headers, request, response, token);
    }

    @RequestMapping(value = "/recreateToken", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> recreateToken(@RequestParam String previousToken, @RequestHeader HttpHeaders headers, HttpServletRequest request, HttpServletResponse response) {
        return businessManagementService.recreateUserToken(headers, request, response, previousToken);
    }
}
