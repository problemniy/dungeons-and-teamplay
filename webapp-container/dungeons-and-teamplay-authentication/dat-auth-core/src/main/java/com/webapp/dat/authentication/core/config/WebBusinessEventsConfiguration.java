package com.webapp.dat.authentication.core.config;

import com.tools.wbe.core.EnableWebBusinessEvents;
import com.tools.wbe.core.component.adapter.abs.AWebBusinessEventsConfigAdapter;
import com.tools.wbe.data.builder.WebBusinessEventsBuilder;
import com.tools.wbe.data.business.WebEventProcessorInvolvement;
import com.tools.wbe.data.business.processor.IWebEventProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@EnableWebBusinessEvents
public class WebBusinessEventsConfiguration extends AWebBusinessEventsConfigAdapter {

    private final List<WebEventProcessorInvolvement<IWebEventProcessor>> wbeProcessors;

    @Autowired
    public WebBusinessEventsConfiguration(List<WebEventProcessorInvolvement<IWebEventProcessor>> wbeProcessors) {
        this.wbeProcessors = wbeProcessors;
    }

    @Override
    protected void configure(WebBusinessEventsBuilder wbe) {
        wbe.setWebEventProcessorsInvolvements(wbeProcessors);
    }
}
