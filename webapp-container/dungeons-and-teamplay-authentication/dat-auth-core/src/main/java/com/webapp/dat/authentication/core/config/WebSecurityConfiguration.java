package com.webapp.dat.authentication.core.config;

import com.webapp.dat.authentication.manage.component.service.AuthenticationUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.provisioning.InMemoryUserDetailsManagerConfigurer;
import org.springframework.security.config.annotation.authentication.configurers.provisioning.UserDetailsManagerConfigurer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final AccessUsersConfiguration accessUsersConfiguration;
    private final AuthenticationUserDetailsService userDetailsService;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public WebSecurityConfiguration(AccessUsersConfiguration accessUsersConfiguration, PasswordEncoder passwordEncoder,
                                    AuthenticationUserDetailsService userDetailsService) {
        this.accessUsersConfiguration = accessUsersConfiguration;
        this.passwordEncoder = passwordEncoder;
        this.userDetailsService = userDetailsService;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        for (AccessUsersConfiguration.ConfigUser user : accessUsersConfiguration.getDefaultUsers()) {
            String[] roles = user.getRoles().toArray(String[]::new);
            String[] authorities = user.getAuthorities().toArray(String[]::new);
            UserDetailsManagerConfigurer<AuthenticationManagerBuilder, InMemoryUserDetailsManagerConfigurer<AuthenticationManagerBuilder>>
                    .UserDetailsBuilder userBuilder = auth.inMemoryAuthentication().passwordEncoder(new PasswordEncoder() {
                @Override
                public String encode(CharSequence rawPassword) {
                    // because users already have encoded passwords
                    return rawPassword.toString();
                }

                @Override
                public boolean matches(CharSequence rawPassword, String encodedPassword) {
                    return rawPassword.toString().equals(encodedPassword);
                }
            })
                    .withUser(user.getName()).password(user.getPassword());
            if (roles.length > 0) {
                userBuilder.roles(roles);
            }
            if (authorities.length > 0) {
                userBuilder.authorities(authorities);
            }
        }
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .formLogin()
                .and()
                .authorizeRequests()
                .antMatchers("/edashboard", "/eureka/**").denyAll()
                .antMatchers("/auth/**", "/wbe/**").hasAnyRole("ADMIN", "SYSTEM")
                .and()
                .logout().permitAll().and().httpBasic()
                .and()
                .csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());
    }
}
