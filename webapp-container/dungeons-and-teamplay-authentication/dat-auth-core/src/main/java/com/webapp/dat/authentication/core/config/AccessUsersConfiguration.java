package com.webapp.dat.authentication.core.config;

import com.webapp.dat.authentication.data.common.utils.AuthorityDataUtils;
import com.webapp.dat.authentication.data.model.DefaultUser;
import com.webapp.dat.authentication.data.role.in.IAuthority;
import com.webapp.dat.authentication.manage.utils.RestAuthenticationHelper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpHeaders;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Configuration
@EnableConfigurationProperties(AccessUsersConfiguration.class)
@PropertySource("classpath:security.properties")
@ConfigurationProperties(prefix = "access.users")
public class AccessUsersConfiguration {

    private ConfigUser eurekaUser;
    private ConfigUser restTemplateUser;

    private List<ConfigUser> defaultUsers = new ArrayList<>();

    public ConfigUser getEurekaUser() {
        return eurekaUser;
    }

    public void setEurekaUser(ConfigUser eurekaUser) {
        this.eurekaUser = eurekaUser;
    }

    public ConfigUser getRestTemplateUser() {
        return restTemplateUser;
    }

    public void setRestTemplateUser(ConfigUser restTemplateUser) {
        this.restTemplateUser = restTemplateUser;
    }

    public List<ConfigUser> getDefaultUsers() {
        return defaultUsers;
    }

    public static class ConfigUser {
        private String name;
        private String password;
        private List<String> roles = new ArrayList<>();
        private List<String> authorities = new ArrayList<>();

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public List<String> getRoles() {
            return roles;
        }

        public List<String> getAuthorities() {
            return authorities;
        }
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Qualifier("securedHttpHeaders")
    public HttpHeaders securedHeaders() {
        return RestAuthenticationHelper.createHeaders(getRestTemplateUser().getName(), getRestTemplateUser().getPassword());
    }

    @Bean("springDefaultUsers")
    @DependsOn("accessUsersConfiguration")
    public Collection<DefaultUser> springDefaultUsers() {
        Collection<DefaultUser> users = new ArrayList<>();
        DefaultUser eurekaUser = new DefaultUser(getEurekaUser().getName(), passwordEncoder().encode(getEurekaUser().getPassword()),
                true, true, true, true, AuthorityDataUtils.parseRoles(getEurekaUser().getRoles()));
        DefaultUser restTemplateUser = new DefaultUser(getRestTemplateUser().getName(), passwordEncoder().encode(getRestTemplateUser().getPassword()),
                true, true, true, true, AuthorityDataUtils.parseRoles(getRestTemplateUser().getRoles()));
        users.add(eurekaUser);
        users.add(restTemplateUser);
        for (AccessUsersConfiguration.ConfigUser configUser : getDefaultUsers()) {
            List<IAuthority> roles = configUser.getRoles().stream().map(AuthorityDataUtils::getAuthority).filter(Objects::nonNull).collect(Collectors.toList());
            DefaultUser defaultUser = new DefaultUser(configUser.getName(), passwordEncoder().encode(configUser.getPassword()), true, true, true, true, roles);
            users.add(defaultUser);
        }
        return users;
    }
}
