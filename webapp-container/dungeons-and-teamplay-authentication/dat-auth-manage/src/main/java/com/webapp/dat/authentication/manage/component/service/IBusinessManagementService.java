package com.webapp.dat.authentication.manage.component.service;

import com.webapp.dat.authentication.data.model.User;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface IBusinessManagementService {

    ResponseEntity<User> createUser(HttpHeaders headers, HttpServletRequest request, HttpServletResponse response, User user);

    ResponseEntity<User> updateUser(HttpHeaders headers, HttpServletRequest request, HttpServletResponse response, User user);

    ResponseEntity<User> getUserByLogin(HttpHeaders headers, HttpServletRequest request, HttpServletResponse response, String login);

    ResponseEntity<User> getUserByToken(HttpHeaders headers, HttpServletRequest request, HttpServletResponse response, String token);

    ResponseEntity<User> recreateUserToken(HttpHeaders headers, HttpServletRequest request, HttpServletResponse response, String previousToken);
}
