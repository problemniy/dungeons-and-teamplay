package com.webapp.dat.authentication.manage.component.service;

import com.webapp.dat.authentication.data.jpa.entity.UserEntity;
import com.webapp.dat.authentication.data.model.User;

public interface IUserService {

    UserEntity convertToUserEntity(User user);

    User convertToUser(UserEntity userEntity);

    void updateUserEntity(UserEntity userEntity, User user);
}
