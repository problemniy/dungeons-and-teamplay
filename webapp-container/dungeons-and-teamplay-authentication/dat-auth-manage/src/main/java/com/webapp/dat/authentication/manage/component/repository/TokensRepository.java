package com.webapp.dat.authentication.manage.component.repository;

import com.webapp.dat.authentication.data.jpa.entity.TokenEntity;
import com.webapp.dat.authentication.data.jpa.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface TokensRepository extends JpaRepository<TokenEntity, Long> {

    @Query("SELECT token FROM TokenEntity token WHERE token.user = ?1")
    UserEntity findByUser(UserEntity user);
}
