package com.webapp.dat.authentication.manage.component.service;

import com.tools.wbe.core.component.service.WebBusinessEventsService;
import com.tools.wbe.data.context.GetRequestContext;
import com.tools.wbe.data.context.GetResponseContext;
import com.tools.wbe.data.context.PostRequestContext;
import com.tools.wbe.data.context.PostResponseContext;
import com.tools.wbe.data.context.PutRequestContext;
import com.tools.wbe.data.context.PutResponseContext;
import com.tools.wbe.manage.executor.GetActionExecutor;
import com.tools.wbe.manage.executor.PostActionExecutor;
import com.tools.wbe.manage.executor.PutActionExecutor;
import com.webapp.dat.authentication.data.constants.GetActionNames;
import com.webapp.dat.authentication.data.constants.PostActionNames;
import com.webapp.dat.authentication.data.constants.PutActionNames;
import com.webapp.dat.authentication.data.constants.WebBusinessActionProperties;
import com.webapp.dat.authentication.data.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class BusinessManagementService implements IBusinessManagementService {

    private final PostActionExecutor postActionExecutor;
    private final PutActionExecutor putActionExecutor;
    private final GetActionExecutor getActionExecutor;
    private final WebBusinessEventsService webBusinessEventsService;

    @Autowired
    public BusinessManagementService(PostActionExecutor postActionExecutor, PutActionExecutor putActionExecutor,
                                     GetActionExecutor getActionExecutor, WebBusinessEventsService webBusinessEventsService) {
        this.postActionExecutor = postActionExecutor;
        this.putActionExecutor = putActionExecutor;
        this.getActionExecutor = getActionExecutor;
        this.webBusinessEventsService = webBusinessEventsService;
    }

    @Override
    public ResponseEntity<User> createUser(HttpHeaders headers, HttpServletRequest request, HttpServletResponse response, User user) {
        return postActionExecutor.prepare(
                () -> webBusinessEventsService.prepareRequestContext(headers, request, user, PostRequestContext.class),
                () -> webBusinessEventsService.prepareResponseContext(new HttpHeaders(), response, PostResponseContext.class))
                .addAction(PostActionNames.CREATE_USER)
                .go().asResponseEntity(User.class);
    }

    @Override
    public ResponseEntity<User> updateUser(HttpHeaders headers, HttpServletRequest request, HttpServletResponse response, User user) {
        return putActionExecutor.prepare(
                () -> webBusinessEventsService.prepareRequestContext(headers, request, user, PutRequestContext.class),
                () -> webBusinessEventsService.prepareResponseContext(new HttpHeaders(), response, PutResponseContext.class))
                .addAction(PutActionNames.UPDATE_USER)
                .addProperty(WebBusinessActionProperties.USER, user)
                .go().asResponseEntity(User.class);
    }

    @Override
    public ResponseEntity<User> getUserByLogin(HttpHeaders headers, HttpServletRequest request, HttpServletResponse response, String login) {
        return getActionExecutor.prepare(
                () -> webBusinessEventsService.prepareRequestContext(headers, request, GetRequestContext.class),
                () -> webBusinessEventsService.prepareResponseContext(new HttpHeaders(), response, GetResponseContext.class))
                .addProperty(WebBusinessActionProperties.LOGIN, login)
                .addAction(GetActionNames.GET_USER_BY_LOGIN)
                .go().asResponseEntity(User.class);
    }

    @Override
    public ResponseEntity<User> getUserByToken(HttpHeaders headers, HttpServletRequest request, HttpServletResponse response, String token) {
        return getActionExecutor.prepare(
                () -> webBusinessEventsService.prepareRequestContext(headers, request, GetRequestContext.class),
                () -> webBusinessEventsService.prepareResponseContext(new HttpHeaders(), response, GetResponseContext.class))
                .addProperty(WebBusinessActionProperties.TOKEN, token)
                .addAction(GetActionNames.GET_USER_BY_TOKEN)
                .go().asResponseEntity(User.class);
    }

    @Override
    public ResponseEntity<User> recreateUserToken(HttpHeaders headers, HttpServletRequest request, HttpServletResponse response, String previousToken) {
        return putActionExecutor.prepare(
                () -> webBusinessEventsService.prepareRequestContext(headers, request, PutRequestContext.class),
                () -> webBusinessEventsService.prepareResponseContext(new HttpHeaders(), response, PutResponseContext.class))
                .addProperty(WebBusinessActionProperties.TOKEN, previousToken)
                .addAction(GetActionNames.RECREATE_USER_TOKEN)
                .go().asResponseEntity(User.class);
    }
}
