package com.webapp.dat.authentication.manage.utils;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.http.HttpHeaders;

import java.nio.charset.StandardCharsets;

public class RestAuthenticationHelper {

    private RestAuthenticationHelper() {
        throw new IllegalStateException("Utility class");
    }

    public static HttpHeaders createHeaders(String username, String password) {
        return new HttpHeaders() {{
            String authentication = username + ":" + password;
            byte[] encodedAuth = Base64.encodeBase64(
                    authentication.getBytes(StandardCharsets.US_ASCII));
            String authHeader = "Basic " + new String(encodedAuth);
            set("Authorization", authHeader);
        }};
    }
}
