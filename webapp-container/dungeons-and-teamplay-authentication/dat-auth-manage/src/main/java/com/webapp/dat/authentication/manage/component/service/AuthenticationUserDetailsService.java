package com.webapp.dat.authentication.manage.component.service;

import com.webapp.dat.authentication.data.model.DefaultUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service("authenticationUserDetailsService")
public class AuthenticationUserDetailsService implements UserDetailsService {

    private final Collection<DefaultUser> springDefaultUsers;

    @Autowired
    public AuthenticationUserDetailsService(@Qualifier("springDefaultUsers") Collection<DefaultUser> springDefaultUsers) {
        this.springDefaultUsers = springDefaultUsers;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDetails userDetails = checkDefaultUser(username);
        if (userDetails == null) {
            throw new UsernameNotFoundException("User was not found!");
        }
        return userDetails;
    }

    private UserDetails checkDefaultUser(String username) {
        return springDefaultUsers.stream().filter((defaultUser) -> defaultUser.getUsername().equals(username)).findAny().orElse(null);
    }
}
