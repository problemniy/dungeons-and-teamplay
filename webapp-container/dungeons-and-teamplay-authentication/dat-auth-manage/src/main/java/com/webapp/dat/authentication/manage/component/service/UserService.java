package com.webapp.dat.authentication.manage.component.service;

import com.webapp.dat.authentication.data.common.utils.AuthorityDataUtils;
import com.webapp.dat.authentication.data.jpa.entity.AccessEntity;
import com.webapp.dat.authentication.data.jpa.entity.InfoEntity;
import com.webapp.dat.authentication.data.jpa.entity.TokenEntity;
import com.webapp.dat.authentication.data.jpa.entity.UserEntity;
import com.webapp.dat.authentication.data.model.AccountData;
import com.webapp.dat.authentication.data.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.ZoneId;

@Component
public class UserService implements IUserService {

    private final SecurityService securityService;

    @Autowired
    public UserService(SecurityService securityService) {
        this.securityService = securityService;
    }

    @Override
    public UserEntity convertToUserEntity(User user) {
        // create user entity
        UserEntity userEntity = new UserEntity();
        userEntity.setLogin(user.getUsername());
        // create info entity
        InfoEntity info = new InfoEntity();
        info.setEmail(user.getAccountData().getEmail());
        info.setFirstName(user.getAccountData().getFirstName());
        info.setLastName(user.getAccountData().getLastName());
        info.setCity(user.getAccountData().getCity());
        info.setCountry(user.getAccountData().getCountry());
        userEntity.setInfo(info);
        // create access entity
        AccessEntity access = new AccessEntity();
        access.setEnabled(user.isEnabled());
        access.setBanned(!user.isAccountNonLocked());
        access.setRoles(AuthorityDataUtils.createAccessLine(user.getAuthorities()));
        access.setUser(userEntity);
        if (user.getPassword() != null) {
            access.setHash(securityService.encodePassword(user.getPassword()));
        }
        userEntity.setAccess(access);
        // create token entity
        TokenEntity token = new TokenEntity();
        token.setUser(userEntity);
        token.setValue(user.getVerificationToken().getValue());
        token.setExpiryDate(user.getVerificationToken().getExpiryDate() != null
                ? user.getVerificationToken().getExpiryDate().atZone(ZoneId.systemDefault()).toLocalDate() : null);
        userEntity.setToken(token);
        // return result
        return userEntity;
    }

    @Override
    public User convertToUser(UserEntity userEntity) {
        AccessEntity access = userEntity.getAccess();
        boolean isExpired = access.getActiveTo() != null && LocalDate.now().isAfter(access.getActiveTo());
        boolean isPasswordExpired = access.getPasswordActiveTo() != null && LocalDate.now().isAfter(access.getPasswordActiveTo());
        User user = new User(userEntity.getLogin(), userEntity.getAccess().getHash(), access.isEnabled(), !isExpired,
                !isPasswordExpired, !access.isBanned(), AuthorityDataUtils.parseAccessLine(userEntity.getAccess().getRoles()));
        // fill user account data
        InfoEntity info = userEntity.getInfo();
        AccountData accountData = user.getAccountData();
        accountData.setEmail(info.getEmail());
        accountData.setFirstName(info.getFirstName());
        accountData.setLastName(info.getLastName());
        accountData.setCity(info.getCity());
        accountData.setCountry(info.getCountry());
        // fill verification token data
        user.getVerificationToken().setValue(userEntity.getToken().getValue());
        user.getVerificationToken().setExpiryDate(userEntity.getToken().getExpiryDate().atStartOfDay(ZoneId.systemDefault()).toInstant());
        // return result
        return user;
    }

    @Override
    public void updateUserEntity(UserEntity userEntity, User user) {
        // merge info entity
        InfoEntity info = userEntity.getInfo();
        info.setEmail(user.getAccountData().getEmail());
        info.setFirstName(user.getAccountData().getFirstName());
        info.setLastName(user.getAccountData().getLastName());
        info.setCity(user.getAccountData().getCity());
        info.setCountry(user.getAccountData().getCountry());
        // merge access entity
        AccessEntity access = userEntity.getAccess();
        access.setEnabled(user.isEnabled());
        access.setBanned(!user.isAccountNonLocked());
        access.setRoles(AuthorityDataUtils.createAccessLine(user.getAuthorities()));
        if (user.getPassword() != null) {
            access.setHash(user.getPassword());
        }
        // merge token entity
        TokenEntity token = userEntity.getToken();
        token.setValue(user.getVerificationToken().getValue());
        token.setExpiryDate(user.getVerificationToken().getExpiryDate().atZone(ZoneId.systemDefault()).toLocalDate());
    }
}
