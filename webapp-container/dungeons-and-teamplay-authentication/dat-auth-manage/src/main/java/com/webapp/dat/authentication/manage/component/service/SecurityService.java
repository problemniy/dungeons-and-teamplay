package com.webapp.dat.authentication.manage.component.service;

import com.webapp.dat.authentication.data.jpa.entity.TokenEntity;
import com.webapp.dat.authentication.data.jpa.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;

@Component
public class SecurityService implements ISecurityService {

    // todo: move to common security configuration
    private static final int TOKEN_DAYS_EXPIRED = 1;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public SecurityService(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public String encodePassword(String password) {
        return passwordEncoder.encode(password);
    }

    @Override
    public TokenEntity createTokenEntity(UserEntity user, String value) {
        TokenEntity tokenEntity = new TokenEntity();
        LocalDateTime expiryDateTime = LocalDateTime.now(ZoneId.systemDefault()).plusDays(TOKEN_DAYS_EXPIRED);
        tokenEntity.setExpiryDate(expiryDateTime.toLocalDate());
        tokenEntity.setUser(user);
        tokenEntity.setValue(value);
        return tokenEntity;
    }
}
