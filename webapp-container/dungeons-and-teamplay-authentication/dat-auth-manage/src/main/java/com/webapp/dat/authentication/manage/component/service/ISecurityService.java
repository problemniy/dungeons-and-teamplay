package com.webapp.dat.authentication.manage.component.service;

import com.webapp.dat.authentication.data.jpa.entity.TokenEntity;
import com.webapp.dat.authentication.data.jpa.entity.UserEntity;

public interface ISecurityService {

    String encodePassword(String password);

    TokenEntity createTokenEntity(UserEntity user, String value);
}
