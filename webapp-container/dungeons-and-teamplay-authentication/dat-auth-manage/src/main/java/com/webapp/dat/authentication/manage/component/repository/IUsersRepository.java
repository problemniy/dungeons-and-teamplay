package com.webapp.dat.authentication.manage.component.repository;

import com.webapp.dat.authentication.data.jpa.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface IUsersRepository extends JpaRepository<UserEntity, Long> {

    @Query("SELECT user FROM UserEntity user WHERE user.login = ?1")
    UserEntity findByLogin(String login);

    @Query("SELECT user FROM UserEntity user LEFT JOIN user.token token WHERE token.value = ?1")
    UserEntity findByToken(String token);
}
