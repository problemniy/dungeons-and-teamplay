package com.webapp.dat.localization.model;

import java.util.Map;

public class EngLanguage extends Language {

    public EngLanguage() {
        super();
    }

    public EngLanguage(Map<String, String> vocabulary) {
        super(vocabulary);
    }

    @Override
    protected String key() {
        return "en";
    }

}
