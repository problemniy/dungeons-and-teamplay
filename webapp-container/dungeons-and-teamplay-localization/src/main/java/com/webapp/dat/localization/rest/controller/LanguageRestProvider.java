package com.webapp.dat.localization.rest.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.webapp.dat.localization.component.service.LocalizationService;
import com.webapp.dat.localization.model.Language;
import com.webapp.dat.localization.spec.enumeration.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/locale/lang")
public class LanguageRestProvider {

    @Autowired
    private LocalizationService localizationService;

    @RequestMapping(path = "/{locale}")
    public @ResponseBody
    String locale(@PathVariable("locale") String locale) {
        JsonObject jsonObj = new JsonObject();
        Locale localeEnum = localizationService.parseLocale(locale);
        if (localeEnum != null) {
            Language language = localizationService.createLanguage(localeEnum);
            language.getVocabulary().put("authLabel", "Авторизация:");
            try {
                String strJson = new ObjectMapper().writeValueAsString(language);
                jsonObj = new Gson().fromJson(strJson, JsonObject.class);
            } catch (JsonProcessingException e) {

            }
        }
        return jsonObj.toString();
    }

}
