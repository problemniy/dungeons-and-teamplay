package com.webapp.dat.localization;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@SpringBootApplication(
        exclude = {
                org.springframework.boot.autoconfigure.gson.GsonAutoConfiguration.class
        })
@EnableDiscoveryClient
@PropertySources({
        @PropertySource("classpath:client.properties"),
        @PropertySource("classpath:database.properties")
})
public class DATLocalizationApp {
        public static void main(String[] args) {
                SpringApplication.run(DATLocalizationApp.class, args);
        }
}
