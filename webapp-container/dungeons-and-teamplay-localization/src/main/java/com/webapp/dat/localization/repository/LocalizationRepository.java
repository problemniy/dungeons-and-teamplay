package com.webapp.dat.localization.repository;

import com.webapp.dat.localization.entity.LocalizationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocalizationRepository extends JpaRepository<LocalizationEntity, Integer> {

    LocalizationEntity findByKeyAndLocale(String key, String locale);

}
