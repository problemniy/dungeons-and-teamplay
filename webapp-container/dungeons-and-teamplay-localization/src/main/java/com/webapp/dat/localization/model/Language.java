package com.webapp.dat.localization.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public abstract class Language implements Serializable {

    final String key;

    final Map<String, String> vocabulary = new HashMap<>();

    public Language() {
        this.key = key();
    }

    public Language(Map<String, String> vocabulary) {
        this.key = key();
        this.vocabulary.putAll(vocabulary);
    }

    protected abstract String key();

    public String getKey() {
        return key;
    }

    public Map<String, String> getVocabulary() {
        return vocabulary;
    }

}
