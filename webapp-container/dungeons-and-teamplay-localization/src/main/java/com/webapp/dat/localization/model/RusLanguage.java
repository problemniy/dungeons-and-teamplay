package com.webapp.dat.localization.model;

import java.util.Map;

public class RusLanguage extends Language {

    public RusLanguage() {
        super();
    }

    public RusLanguage(Map<String, String> vocabulary) {
        super(vocabulary);
    }

    @Override
    protected String key() {
        return "ru";
    }

}
