package com.webapp.dat.localization.component.service;

import com.webapp.dat.localization.model.EngLanguage;
import com.webapp.dat.localization.model.Language;
import com.webapp.dat.localization.model.RusLanguage;
import com.webapp.dat.localization.repository.LocalizationRepository;
import com.webapp.dat.localization.spec.enumeration.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LocalizationService {

    @Autowired
    LocalizationRepository localizationRepository;

    public Language createLanguage(Locale locale) {
        Language language;
        switch (locale) {
            case RUS:
                language = new RusLanguage();
                break;
            default:
                language = new EngLanguage();
                break;
        }
        return language;
    }

    public Locale parseLocale(String key) {
        if (key.equals(Locale.RUS.getKey())) {
            return Locale.RUS;
        } else if (key.equals(Locale.ENG.getKey())) {
            return Locale.ENG;
        }
        return null;
    }

}
