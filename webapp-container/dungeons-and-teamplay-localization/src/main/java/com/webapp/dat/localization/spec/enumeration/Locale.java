package com.webapp.dat.localization.spec.enumeration;

public enum Locale {

    RUS("ru"),
    ENG("en");

    private final String key;

    Locale(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

}
