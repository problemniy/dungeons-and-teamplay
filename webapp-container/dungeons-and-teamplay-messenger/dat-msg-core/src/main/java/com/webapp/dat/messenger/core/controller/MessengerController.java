package com.webapp.dat.messenger.core.controller;

import com.webapp.dat.messenger.mail.component.service.MailBusinessManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/msg")
public class MessengerController {

    private final MailBusinessManagementService mailBusinessManagementService;

    @Autowired
    public MessengerController(MailBusinessManagementService mailBusinessManagementService) {
        this.mailBusinessManagementService = mailBusinessManagementService;
    }

    @RequestMapping(path = "/confirm", method = RequestMethod.HEAD)
    public HttpHeaders sendConfirmationLink(@RequestParam String userName, @RequestParam String email, @RequestParam String link, @RequestParam String locale,
                                            @RequestHeader HttpHeaders headers, HttpServletRequest req, HttpServletResponse resp) {
        return mailBusinessManagementService.sendConfirmationLink(userName, email, link, locale, headers, req, resp);
    }
}
