package com.webapp.dat.messenger.core.component.wbe.processor;

import com.tools.wbe.data.business.delegate.IWebBusinessProcess;
import com.tools.wbe.data.business.processor.IHeadActionProcessor;
import com.tools.wbe.data.context.HeadRequestContext;
import com.tools.wbe.data.context.HeadResponseContext;
import com.webapp.dat.messenger.data.constants.WbeActionProperties;
import com.webapp.dat.messenger.mail.component.service.IMailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("sendConfirmationEmailProcessor")
public class SendConfirmationEmailProcessor implements IHeadActionProcessor {

    private final IMailService mailService;

    @Autowired
    public SendConfirmationEmailProcessor(IMailService mailService) {
        this.mailService = mailService;
    }

    @Override
    public void execute(HeadRequestContext request, HeadResponseContext response, IWebBusinessProcess businessProcess) {
        String userName = businessProcess.properties().getProperty(WbeActionProperties.USER_NAME, String.class);
        String recipientAddress = businessProcess.properties().getProperty(WbeActionProperties.EMAIL, String.class);
        String link = businessProcess.properties().getProperty(WbeActionProperties.CONFIRMATION_LINK, String.class);
        if (userName == null || recipientAddress == null || link == null) {
            return;
        }
        String subject = "Registration Confirmation";
        //todo: make a message for email
        String message = ">>Test message for " + "userName " + link;
        mailService.sendSimpleMessage(recipientAddress, subject, message);
    }
}
