package com.webapp.dat.messenger.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@SpringBootApplication(
        exclude = {
                org.springframework.boot.autoconfigure.gson.GsonAutoConfiguration.class
        })
@EnableEurekaClient
@ComponentScan({
        "com.webapp.dat.messenger.core",
        "com.webapp.dat.messenger.mail",
        "com.webapp.dat.messenger.manage"
})
@ImportResource("classpath:/context/wbe-processors.config.xml")
@PropertySources({
        @PropertySource("classpath:security.properties"),
        @PropertySource("classpath:client.properties"),
        @PropertySource("classpath:database.properties")
})
public class DATMessengerApp {

    public static void main(String[] args) {
        SpringApplication.run(DATMessengerApp.class, args);
    }
}
