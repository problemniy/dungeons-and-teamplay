package com.webapp.dat.messenger.core.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableConfigurationProperties(AccessUsersConfiguration.class)
@PropertySource("classpath:security.properties")
@ConfigurationProperties(prefix = "access.users")
public class AccessUsersConfiguration {

    private ConfigUser eurekaUser;

    private List<ConfigUser> defaultUsers = new ArrayList<>();

    public ConfigUser getEurekaUser() {
        return eurekaUser;
    }

    public void setEurekaUser(ConfigUser eurekaUser) {
        this.eurekaUser = eurekaUser;
    }

    public List<ConfigUser> getDefaultUsers() {
        return defaultUsers;
    }

    public static class ConfigUser {
        private String name;
        private String password;
        private List<String> roles = new ArrayList<>();
        private List<String> authorities = new ArrayList<>();

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public List<String> getRoles() {
            return roles;
        }

        public List<String> getAuthorities() {
            return authorities;
        }
    }
}
