package com.webapp.dat.messenger.core.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    AccessUsersConfiguration accessUsersConfiguration;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        PasswordEncoder encoder = passwordEncoder();
        for (AccessUsersConfiguration.ConfigUser user : accessUsersConfiguration.getDefaultUsers()) {
            String[] roles = user.getRoles().toArray(String[]::new);
            String[] authorities = user.getAuthorities().toArray(String[]::new);
            auth.inMemoryAuthentication().passwordEncoder(encoder)
                    .withUser(user.getName()).password(encoder.encode(user.getPassword())).roles(roles).authorities(authorities);
        }
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .formLogin()
                .and()
                .authorizeRequests()
                .antMatchers("/eureka/**").denyAll()
                .antMatchers("/edashboard").denyAll()
                .and()
                .logout().permitAll().and().httpBasic()
                .and()
                .csrf().ignoringAntMatchers("/msg/confirm");
    }
}
