package com.webapp.dat.messenger.data.constants;

public final class WbeActionNames {

    private WbeActionNames() {
    }

    public final static class HeadActions {
        public static final String EMAIL_CONFIRMATION = "EMAIL_CONFIRMATION";

        private HeadActions() {
        }
    }
}
