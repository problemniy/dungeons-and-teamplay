package com.webapp.dat.messenger.data.constants;

public final class WbeActionProperties {

    public static final String USER_NAME = "userName";
    public static final String EMAIL = "email";
    public static final String CONFIRMATION_LINK = "confirmationLink";
    public static final String LOCALE = "locale";

    private WbeActionProperties() {
    }
}
