package com.webapp.dat.messenger.mail.component.service;

import com.tools.wbe.core.component.service.WebBusinessEventsService;
import com.tools.wbe.data.context.HeadRequestContext;
import com.tools.wbe.data.context.HeadResponseContext;
import com.tools.wbe.manage.executor.HeadActionExecutor;
import com.webapp.dat.messenger.data.constants.WbeActionNames;
import com.webapp.dat.messenger.data.constants.WbeActionProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

@Service
public class MailBusinessManagementService implements IMailBusinessManagementService {

    private final HeadActionExecutor headActionExecutor;
    private final WebBusinessEventsService webBusinessEventsService;

    @Autowired
    public MailBusinessManagementService(HeadActionExecutor headActionExecutor, WebBusinessEventsService webBusinessEventsService) {
        this.headActionExecutor = headActionExecutor;
        this.webBusinessEventsService = webBusinessEventsService;
    }

    @Override
    public HttpHeaders sendConfirmationLink(String userName, String email, String link, String locale, HttpHeaders headers, HttpServletRequest request, HttpServletResponse response) {
        return headActionExecutor.prepare(
                () -> webBusinessEventsService.prepareRequestContext(headers, request, HeadRequestContext.class),
                () -> webBusinessEventsService.prepareResponseContext(response, HeadResponseContext.class))
                .addAction(WbeActionNames.HeadActions.EMAIL_CONFIRMATION)
                .addProperties(new HashMap<>() {{
                    put(WbeActionProperties.USER_NAME, userName);
                    put(WbeActionProperties.EMAIL, email);
                    put(WbeActionProperties.CONFIRMATION_LINK, link);
                    put(WbeActionProperties.LOCALE, locale);
                }})
                .go().asHeaders();
    }
}
