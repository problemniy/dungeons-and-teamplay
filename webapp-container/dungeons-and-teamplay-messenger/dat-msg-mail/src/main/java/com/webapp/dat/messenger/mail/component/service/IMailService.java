package com.webapp.dat.messenger.mail.component.service;

public interface IMailService {

    void sendSimpleMessage(String recipientAddress, String subject, String message);
}
