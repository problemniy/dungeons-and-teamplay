package com.webapp.dat.messenger.mail.component.service;

import org.springframework.http.HttpHeaders;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface IMailBusinessManagementService {

    HttpHeaders sendConfirmationLink(String userName, String email, String link, String locale, HttpHeaders headers, HttpServletRequest request, HttpServletResponse response);
}
