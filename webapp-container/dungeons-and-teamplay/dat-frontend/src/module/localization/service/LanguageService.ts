import {inject, injectable} from "inversify";
import {i18n} from "../../../config/i18n.config";
import {RestQueryBuilder} from "../../../common/RestQueryBuilder";
import {localizationRestPaths} from "../constants/url/LocalizationRestPaths";
import {localizationLangConstants} from "../constants/LocalizationLangConstants";

@injectable()
class LanguageService {
    @inject(RestQueryBuilder) private _restQueryBuilder!: RestQueryBuilder;

    private _loadedLanguages: [string] = [localizationLangConstants.LOCALE.EN];

    public loadLocalization(lang: string): Promise<string | void> {
        if (i18n.locale !== lang && !this._loadedLanguages.includes(lang)) {
            let restQuery = this._restQueryBuilder
                .prepare(localizationRestPaths)
                .add(localizationRestPaths.LANG)
                .add(lang)
                .build();
            return Promise.resolve(
                fetch(restQuery, {
                    method: "GET"
                }).then(
                    (result): void => {
                        result.json().then(
                            (data): void => {
                                let actualLocale = lang.replace(/\//, "");
                                this._loadedLanguages.push(actualLocale);
                                i18n.setLocaleMessage(actualLocale, data.vocabulary);
                            }
                        );
                    }
                )
            );
        }
        return Promise.resolve(lang);
    }
}

export {LanguageService};
