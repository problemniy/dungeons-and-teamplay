import {inject, injectable} from "inversify";
import {countries} from "countries-list";
import {RestQueryBuilder} from "../../../common/RestQueryBuilder";

@injectable()
class InformationService {
    @inject(RestQueryBuilder) private _restQueryBuilder!: RestQueryBuilder;

    public getCountries(): { key: string; name: string }[] {
        let result: { key: string; name: string }[] = [];
        Object.entries(countries).forEach((k: any): void => {
            result.push({
                key: k[0],
                name: k[1].name
            });
        });
        return result;
    }
}

export {InformationService};
