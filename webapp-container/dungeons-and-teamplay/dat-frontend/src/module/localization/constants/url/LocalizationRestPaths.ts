import {RestPaths} from "../../../../constants/url/RestPaths";

class LocalizationRestPaths implements RestPaths {
    public readonly GATE: string = "/locale";

    public readonly LANG: string = "/lang";
    public readonly RU: string = "/ru";
    public readonly EN: string = "/en";
}

let localizationRestPaths: LocalizationRestPaths = new LocalizationRestPaths();

export {localizationRestPaths};
