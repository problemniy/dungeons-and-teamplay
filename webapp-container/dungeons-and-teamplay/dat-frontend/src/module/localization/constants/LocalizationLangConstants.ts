class LocalizationLangConstants {
    public readonly LOCALE = {
        EN: "en",
        RU: "ru"
    };
}

let localizationLangConstants: LocalizationLangConstants = new LocalizationLangConstants();

export {localizationLangConstants};
