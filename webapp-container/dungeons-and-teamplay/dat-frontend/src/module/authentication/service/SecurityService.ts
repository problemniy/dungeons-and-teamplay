import {injectable} from "inversify";
import PasswordValidator from "password-validator";

const schema = new PasswordValidator();
schema
    .is().min(8)            // Minimum length 8
    .is().max(20)           // Maximum length 100
    .has().uppercase()      // Must have uppercase letters
    .has().lowercase()      // Must have lowercase letters
    .has().digits()         // Must have digits
    .has().not().spaces()   // Should not have spaces
    .is().not()
    .oneOf([                // Blacklist values
        "Passw0rd",
        "Password1",
        "Password12",
        "Password123",
        "Password1234",
        "Password12345",
        "Pa55word",
        "Pa55w0rd"
    ]);

@injectable()
class SecurityService {
    public validatePasswordGetErrors(pass: string): string[] {
        let errors: string[] = schema.validate(pass, {list: true});
        return errors;
    }

    public validatePassword(pass: string): boolean {
        return schema.validate(pass);
    }
}

export {SecurityService};
