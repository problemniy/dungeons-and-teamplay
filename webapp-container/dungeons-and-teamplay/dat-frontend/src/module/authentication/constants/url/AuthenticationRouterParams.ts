class AuthenticationRouterParams {
    public readonly AUTH_LOG: string = "log";
    public readonly AUTH_REG: string = "reg";
    public readonly AUTH_CONFIRM: string = "confirm";
}

let authenticationRouterParams: AuthenticationRouterParams = new AuthenticationRouterParams();

export {authenticationRouterParams};
