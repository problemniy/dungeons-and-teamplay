import {RestPaths} from "../../../../constants/url/RestPaths";

class AuthenticationRestPaths implements RestPaths {
    public readonly GATE: string = "/auth";
    public readonly REGISTER_USER: string = "/reg";
    public readonly USER_EXISTS: string = "/userExists";
    public readonly SIGN_IN: string = "/sign";
    public readonly REGISTRATION_CONFIRM: string = "/confirm";
    public readonly RECREATE_TOKEN: string = "/recreateToken";
}

let authenticationRestPaths: AuthenticationRestPaths = new AuthenticationRestPaths();

export {authenticationRestPaths};
