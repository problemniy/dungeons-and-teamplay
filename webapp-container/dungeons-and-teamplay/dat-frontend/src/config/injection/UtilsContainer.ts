import {Container} from "inversify";
import {RegExpValidator} from "../../common/utils/RegExpValidator";
import {CommonUtils} from "../../common/utils/CommonUtils";
import {CookieUtils} from "../../common/utils/CookieUtils";
import {IInjectionContainer} from "./root/IInjectionContainer";

class UtilsContainer implements IInjectionContainer {
    public apply(container: Container): void {
        container.bind<RegExpValidator>(RegExpValidator).toSelf();
        container.bind<CommonUtils>(CommonUtils).toSelf();
        container.bind<CookieUtils>(CookieUtils).toSelf();
    }
}

export {UtilsContainer};
