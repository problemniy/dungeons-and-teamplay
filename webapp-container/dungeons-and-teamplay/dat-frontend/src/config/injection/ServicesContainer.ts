import {Container} from "inversify";
import {RestQueryBuilder} from "../../common/RestQueryBuilder";
import {LanguageService} from "../../module/localization/service/LanguageService";
import {InformationService} from "../../module/localization/service/InformationService";
import {SecurityService} from "../../module/authentication/service/SecurityService";
import {IInjectionContainer} from "./root/IInjectionContainer";

class ServicesContainer implements IInjectionContainer {
    public apply(container: Container): void {
        container.bind<RestQueryBuilder>(RestQueryBuilder).toSelf();
        container.bind<LanguageService>(LanguageService).toSelf();
        container.bind<InformationService>(InformationService).toSelf();
        container.bind<SecurityService>(SecurityService).toSelf();
    }
}

export {ServicesContainer};
