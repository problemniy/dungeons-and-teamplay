import {Container} from "inversify"

export interface IInjectionContainer {
    apply(container: Container): void;
}
