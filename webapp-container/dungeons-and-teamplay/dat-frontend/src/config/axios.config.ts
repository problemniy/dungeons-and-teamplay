import axios from "axios";

axios.defaults.baseURL = "http://localhost:8761";
//axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
axios.defaults.headers.post["Content-Type"] = "application/json";

export default axios;
