export const getters = {
    isLoading(state: any): boolean {
        return state.loading;
    },
    isConfirmationPageEnabled(state: any): boolean {
        return state.isConfirmationPageEnabled;
    }
};
