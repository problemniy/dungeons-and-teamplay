import {ActionTree} from "vuex";
import {authenticationActions} from "../actions/authentication-actions";
import {messengerActions} from "../actions/messenger-actions";

const moduleActions: ActionTree<any, any>[] = [
    authenticationActions,
    messengerActions
];

function calculateActions(): ActionTree<any, any> {
    let result: ActionTree<any, any> = {};
    moduleActions.forEach((spec: ActionTree<any, any>): ActionTree<any, any> => result = Object.assign(result, spec));
    return result;
}

export const actions: ActionTree<any, any> = calculateActions();
