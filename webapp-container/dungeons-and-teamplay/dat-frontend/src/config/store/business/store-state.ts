import {localStoreAttributes} from "../../../constants/LocalStoreAttributes";

export const state = {
    isAuthenticated: localStorage.getItem(localStoreAttributes.IS_AUTHORIZED),
    isConfirmationPageEnabled: false,
    loading: false
};
