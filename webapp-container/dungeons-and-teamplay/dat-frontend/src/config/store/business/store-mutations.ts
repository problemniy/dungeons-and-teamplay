import {localStoreAttributes} from "../../../constants/LocalStoreAttributes";

export const mutations = {
    setAuthenticated(state: any, payload: string): void {
        localStorage.setItem(localStoreAttributes.IS_AUTHORIZED, payload);
    },
    clearAuthenticated(): void {
        localStorage.removeItem(localStoreAttributes.IS_AUTHORIZED);
    },
    enableConfirmationPage(state: any, payload: boolean): void {
        state.isConfirmationPageEnabled = payload;
    },
    setLoading(state: any, payload: boolean): void {
        state.loading = payload;
    }
};
