import Vue from "vue";
import Vuex from "vuex";
import {actions} from "./business/store-actions";
import {mutations} from "./business/store-mutations";
import {state} from "./business/store-state";
import {getters} from "./business/store-getters";

Vue.use(Vuex);

export const store = new Vuex.Store({
    actions, mutations, state, getters
});
