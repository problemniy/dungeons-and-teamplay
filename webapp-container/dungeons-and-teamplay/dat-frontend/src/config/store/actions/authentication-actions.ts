import {ActionTree} from "vuex";
import axios from "../../axios.config";
import container from "../../inversify.config";
import {RestQueryBuilder} from "../../../common/RestQueryBuilder";
import {CookieUtils} from "../../../common/utils/CookieUtils";
import {CommonUtils} from "../../../common/utils/CommonUtils";
import {authenticationRestPaths} from "../../../module/authentication/constants/url/AuthenticationRestPaths";
import User from "../../../common/model/User";

const _restQueryBuilder: RestQueryBuilder = container.get<RestQueryBuilder>(RestQueryBuilder);
const _commonUtils: CommonUtils = container.get<CommonUtils>(CommonUtils);
const _cookieUtils: CookieUtils = container.get<CookieUtils>(CookieUtils);

export const authenticationActions: ActionTree<any, any> = {
    regConfirm(store: { commit: any }, payload: { token: string }): Promise<any> {
        let token: string = payload.token;
        let url: string = _restQueryBuilder.prepare(authenticationRestPaths).add(authenticationRestPaths.REGISTRATION_CONFIRM)
            .addParameter("token", token).build();
        store.commit("setLoading", true);
        return new Promise<any>((resolve: any, reject: any): void => {
            axios.get(url)
                .then((res: any): void => {
                    store.commit("setLoading", false);
                    resolve(res.data);
                })
                .catch((error: any): void => {
                    store.commit("setLoading", false);
                    reject(error.response);
                });
        });
    },
    regUser(store: { commit: any }, payload: { user: User }): Promise<any> {
        let user: User = payload.user;
        let url: string = _restQueryBuilder.prepare(authenticationRestPaths).add(authenticationRestPaths.REGISTER_USER).build();
        let preparedJson = _commonUtils.removeMetaVariableIdentifiers(JSON.stringify(user));
        store.commit("setLoading", true);
        return new Promise<any>((resolve: any, reject: any): void => {
            axios.post(url, preparedJson).then((res: any): void => {
                //todo: need to convert res.data to User object and commit to store
                //store.commit("setUser", res.data);
                store.commit("setLoading", false);
                resolve(res);
            }).catch((error: any): void => {
                store.commit("setLoading", false);
                reject(error.response);
            });
        });
    },
    recreateToken(store: { commit: any }, payload: { previousToken: string }): Promise<any> {
        store.commit("setLoading", true);
        let url: string = _restQueryBuilder.prepare(authenticationRestPaths).add(authenticationRestPaths.RECREATE_TOKEN).build();
        return new Promise<any>((resolve: any, reject: any): void => {
            axios.put(url, payload.previousToken,
                {
                    headers: {
                        //todo: create json content type as default for axios
                        "Content-Type": "application/json"
                    }
                }
            )
                .then((res: any): void => {
                    store.commit("setLoading", false);
                    resolve(res.data);
                })
                .catch((error: any): void => {
                    store.commit("setLoading", false);
                    reject(error.response);
                });
        });
    },
    userSignIn(store: { commit: any }, payload: { username: string; password: string }): Promise<any> {
        let formData = new FormData();
        formData.append("username", payload.username);
        formData.append("password", payload.password);
        let _csrf = _cookieUtils.getCookie("XSRF-TOKEN");
        if (_csrf) {
            formData.append("_csrf", _csrf);
        }
        store.commit("setLoading", true);
        let url: string = _restQueryBuilder.prepare(authenticationRestPaths).add(authenticationRestPaths.SIGN_IN).build();
        return new Promise<any>((resolve: any, reject: any): void => {
            axios.post(url, formData)
                .then((res: any): void => {
                    store.commit("setLoading", false);
                    store.commit("setAuthenticated", true);
                    //todo: implement event bus
                    //EventBus.$emit('authenticated', 'User authenticated')
                    resolve(res);
                })
                .catch((error: any): void => {
                    store.commit("setLoading", false);
                    store.commit("clearAuthenticated");
                    reject(error.response.data);
                });
        });
    },
    userExists(store: { commit: any }, payload: { login: string }): Promise<boolean> {
        store.commit("setLoading", true);
        let url: string = _restQueryBuilder.prepare(authenticationRestPaths).add(authenticationRestPaths.USER_EXISTS).addParameter("login", payload.login).build();
        return new Promise<any>((resolve: any): void => {
            axios.get(url)
                .then((res: any): void => {
                    store.commit("setLoading", false);
                    resolve(res.data);
                })
                .catch((): void => {
                    store.commit("setLoading", false);
                    resolve(false);
                });
        });
    }
};
