import {Container} from "inversify";
import "reflect-metadata";
import {IInjectionContainer} from "./injection/root/IInjectionContainer";
import {ServicesContainer} from "./injection/ServicesContainer";
import {UtilsContainer} from "./injection/UtilsContainer";

const containers: IInjectionContainer[] = [
    new ServicesContainer() as IInjectionContainer,
    new UtilsContainer() as IInjectionContainer
];

const container: Container = new Container();

containers.forEach((injectionContainer): void => injectionContainer.apply(container));

export default container;
