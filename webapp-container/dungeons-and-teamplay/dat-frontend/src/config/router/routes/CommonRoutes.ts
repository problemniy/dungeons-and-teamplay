import {RouteConfig} from "vue-router";
import GameManager from "../../../module/game/GameManager.vue";
import {IModuleRoutes} from "./root/ModuleRoutes";

class CommonRoutes implements IModuleRoutes {
    public getRoutes(): RouteConfig[] {
        return [
            {
                path: "/",
                redirect: "/game"
            },
            {
                path: "/game",
                name: "game",
                component: GameManager,
                meta: {withAuthentication: true}
            }
        ];
    }

    public configureRouter(): void {
        return;
    }
}

export const commonRoutes = new CommonRoutes();
