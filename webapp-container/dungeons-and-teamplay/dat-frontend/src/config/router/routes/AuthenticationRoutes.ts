import VueRouter, {RouteConfig} from "vue-router";
import {CONFLICT, NOT_ACCEPTABLE} from "http-status-codes";
import ServiceAuthentication from "../../../module/authentication/ServiceAuthentication.vue";
import Authentication from "../../../module/authentication/Authentication.vue";
import CheckRegistration from "../../../module/authentication/component/CheckRegistration.vue";
import {store} from "../../store/store.config";
import {localStoreAttributes} from "../../../constants/LocalStoreAttributes";
import {storeDispatchEvents} from "../../../constants/StoreDispatchEvents";
import {IModuleRoutes} from "./root/ModuleRoutes";

class AuthenticationRoutes implements IModuleRoutes {
    public getRoutes(): RouteConfig[] {
        return [
            {
                path: "/sauth/sign",
                name: "service-authentication",
                component: ServiceAuthentication,
                meta: {withAuthentication: false}
            },
            {
                path: "/auth",
                name: "authentication",
                redirect: {name: "authentication-type", params: {type: "log"}}
            },
            {
                path: "/auth/:type",
                name: "authentication-type",
                component: Authentication,
                meta: {withAuthentication: false}
            },
            {
                path: "/checkReg",
                name: "check-registration",
                component: CheckRegistration,
                props: true,
                meta: {withAuthentication: false}
            }
        ];
    }

    public configureRouter(router: VueRouter): void {
        router.beforeEach((to: any, from: any, next: any): void => {
            if (this.isRegConfirmation(router, to)) {
                return;
            }
            const isAuthenticationRequired: boolean = to.matched.some((record: any): boolean => record.meta.withAuthentication);
            const isAuthorizedUser = localStorage.getItem(localStoreAttributes.IS_AUTHORIZED);
            if (isAuthenticationRequired && !isAuthorizedUser) {
                next("/auth/log");
            } else if ((to.path === "/auth/confirm" || to.path === "/checkReg") && !store.getters.isConfirmationPageEnabled) {
                next("/game");
            } else next();
        });
    }

    private isRegConfirmation(router: VueRouter, to: any): boolean {
        let result = false;
        if (to.params.type && to.params.type === "registrationConfirm") {
            let token = to.query.token;
            store.dispatch(storeDispatchEvents.REGISTRATION_CONFIRM, {token: token}).then((): void => {
                store.commit("enableConfirmationPage", true);
                router.replace("/checkReg");
            }).catch((error: any): void => {
                if (error.status == NOT_ACCEPTABLE || error.status == CONFLICT) {
                    store.commit("enableConfirmationPage", true);
                    router.replace({
                        name: "check-registration",
                        params: {
                            responseCode: error.status == NOT_ACCEPTABLE ? NOT_ACCEPTABLE.toString() : CONFLICT.toString(),
                            previousToken: token
                        }
                    });
                } else router.replace("/game");
            });
            result = true;
        }
        return result;
    }
}

export const authenticationRoutes = new AuthenticationRoutes();
