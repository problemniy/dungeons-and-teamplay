import VueRouter, {RouteConfig} from "vue-router";

export interface IModuleRoutes {
    getRoutes(): RouteConfig[];

    configureRouter(router: VueRouter): void;
}
