import Vue from "vue";
import VueRouter, {RouteConfig} from "vue-router";
import {commonRoutes} from "./routes/CommonRoutes";
import {authenticationRoutes} from "./routes/AuthenticationRoutes";
import {IModuleRoutes} from "./routes/root/ModuleRoutes";

Vue.use(VueRouter);

let moduleRoutes: IModuleRoutes[] = [
    commonRoutes,
    authenticationRoutes
];

let allRoutes: RouteConfig[] = [];
moduleRoutes.forEach((routes: IModuleRoutes): void => {
    allRoutes = new Array<RouteConfig>().concat(allRoutes, routes.getRoutes());
});

export const router = new VueRouter({
    mode: "history",
    routes: allRoutes
});

moduleRoutes.forEach((moduleRoutes: IModuleRoutes): void => {
    moduleRoutes.configureRouter(router);
});
