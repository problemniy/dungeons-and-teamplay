import AccountData from "./AccountData";
import AccessData from "./AccessData";

export default class User {
    private _login: string = "";
    private _password: string = "";
    private readonly _accountData: AccountData = new AccountData();
    private readonly _accessData: AccessData = new AccessData();

    public get login(): string {
        return this._login;
    }

    public set login(value: string) {
        this._login = value;
    }

    public get password(): string {
        return this._password;
    }

    public set password(value: string) {
        this._password = value;
    }

    public get accountData(): AccountData {
        return this._accountData;
    }

    public get accessData(): AccessData {
        return this._accessData;
    }
}
