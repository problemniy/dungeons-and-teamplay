export default class AccessData {
    private _enabled: boolean = false;
    private _accountNonExpired: boolean = true;
    private _credentialsNonExpired: boolean = true;
    private _accountNonLocked: boolean = true;
    private _authorities: string[] = [];

    public get enabled(): boolean {
        return this._enabled;
    }

    public set enabled(value: boolean) {
        this._enabled = value;
    }

    public get accountNonExpired(): boolean {
        return this._accountNonExpired;
    }

    public set accountNonExpired(value: boolean) {
        this._accountNonExpired = value;
    }

    public get credentialsNonExpired(): boolean {
        return this._credentialsNonExpired;
    }

    public set credentialsNonExpired(value: boolean) {
        this._credentialsNonExpired = value;
    }

    public get accountNonLocked(): boolean {
        return this._accountNonLocked;
    }

    public set accountNonLocked(value: boolean) {
        this._accountNonLocked = value;
    }

    public get authorities(): string[] {
        return this._authorities;
    }

    public set authorities(value: string[]) {
        this._authorities = value;
    }
}