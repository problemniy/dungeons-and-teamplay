import {injectable} from "inversify";
import {RestPaths} from "../constants/url/RestPaths";

@injectable()
export class RestQueryBuilder {
    private _rest: string = "";

    public prepare(paths: RestPaths): RestQueryBuilder {
        this._rest = paths.GATE;
        return this;
    }

    public add(path: string): RestQueryBuilder {
        let actualPath = path.replace(/\s/g, "");
        actualPath = actualPath.replace(/\\/, "/");
        if (!actualPath.startsWith("/")) {
            actualPath = "/".concat(actualPath);
        }
        this._rest = this._rest.concat(actualPath);
        return this;
    }

    public addParameter(key: string, value: string): RestQueryBuilder {
        this._rest = this._rest.concat((this._rest.includes("?") ? "&" : "?") + key + "=" + value);
        return this;
    }

    public build(): string {
        return this._rest;
    }
}
