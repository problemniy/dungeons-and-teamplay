import {createDecorator} from "vue-class-component";

export const ValidationMethod = function (propertyPath: string): any {
    return createDecorator((target: any, key: string): void => {
        let property = target.validations;
        if (property) {
            let keys = propertyPath.replace(/\s/g, "").split(".");
            for (let i = 0; i < keys.length; i++) {
                property = property[keys[i]];
            }
            if (property) {
                property[key] = target.methods[key];
            }
        }
    });
};
