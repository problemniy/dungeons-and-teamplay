import {injectable} from "inversify";
import validator from "validator";

@injectable()
class RegExpValidator {
    public checkMinMaxSize(content: string, min?: number, max?: number): boolean {
        let sizeIsValid = true;
        if (min != undefined)
            sizeIsValid = content.length >= min;
        if (max != undefined)
            sizeIsValid = sizeIsValid && content.length <= max;
        return sizeIsValid;
    }

    public checkLettersNumbersOnly(content: string, locales: string[] = []): boolean {
        return validator.isAlphanumeric(content, locales.length == 0 ? undefined : locales);
    }

    public checkNumbersOnly(content: string): boolean {
        return validator.isNumeric(content);
    }

    public checkLettersOnly(content: string, locales: string[] = []): boolean {
        return validator.isAlpha(content, locales.length == 0 ? undefined : locales);
    }

    public checkEmail(content: string): boolean {
        return validator.isEmail(content);
    }
}

export {RegExpValidator};
