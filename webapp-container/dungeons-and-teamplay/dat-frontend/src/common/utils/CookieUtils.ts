import {injectable} from "inversify";

@injectable()
class CookieUtils {
    public getCookie(name: string): string | null {
        const nameLenPlus = name.length + 1;
        return document.cookie
            .split(";")
            .map((c): string => c.trim())
            .filter((cookie): boolean => {
                return cookie.substring(0, nameLenPlus) === `${name}=`;
            })
            .map((cookie): string => {
                return decodeURIComponent(cookie.substring(nameLenPlus));
            })[0] || null;
    }
}

export {CookieUtils};
