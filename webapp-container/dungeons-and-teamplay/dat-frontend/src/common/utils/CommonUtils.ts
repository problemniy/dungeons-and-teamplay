import {injectable} from "inversify";

@injectable()
class CommonUtils {
    public removeMetaVariableIdentifiers(container: string): string {
        return container.replace(/[_-]/g, "");
    }
}

export {CommonUtils};
