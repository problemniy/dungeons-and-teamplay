import "./config/reg-hooks.config";
import Vue, {VNode} from "vue";
import BootstrapVue from "bootstrap-vue";
import Vuelidate from "vuelidate";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import VueObserveVisibility from "vue-observe-visibility";
import axios from "./config/axios.config";
import Application from "./Application.vue";
import {i18n} from "./config/i18n.config";
import {router} from "./config/router/router.config";
import {store} from "./config/store/store.config";

declare module "vue/types/vue" {
    interface Vue {
        $axios: any;
        $v: any;
    }
}

declare module "vue/types/options" {
    interface ComponentOptions<V extends Vue> {
        validations?: {};
    }
}

Vue.use(BootstrapVue);
Vue.use(VueObserveVisibility);
Vue.use(Vuelidate);

Vue.config.productionTip = false;

Vue.prototype.$axios = axios;

new Vue({
    i18n,
    router,
    store,
    render: (h: any): VNode => h(Application),
}).$mount("#application");
