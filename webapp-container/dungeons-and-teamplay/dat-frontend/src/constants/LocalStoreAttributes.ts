class LocalStoreAttributes {
    public readonly IS_AUTHORIZED: string = "authorized";
}

let localStoreAttributes: LocalStoreAttributes = new LocalStoreAttributes();

export {localStoreAttributes};
