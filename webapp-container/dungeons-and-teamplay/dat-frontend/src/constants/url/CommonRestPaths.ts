import {RestPaths} from "./RestPaths";

class CommonRestPaths implements RestPaths {
    public readonly GATE: string = "";
}

let commonRestPaths: CommonRestPaths = new CommonRestPaths();

export {commonRestPaths};
