class StoreDispatchEvents {
    public readonly REGISTER_USER: string = "regUser";
    public readonly USER_SIGN_IN: string = "userSignIn";
    public readonly CHECK_USER_EXISTS: string = "userExists";
    public readonly REGISTRATION_CONFIRM: string = "regConfirm";
    public readonly RECREATE_TOKEN: string = "recreateToken";
}

let storeDispatchEvents: StoreDispatchEvents = new StoreDispatchEvents();

export {storeDispatchEvents};
