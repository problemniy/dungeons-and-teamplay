// vue.config.js
module.exports = {
    outputDir: 'build/dist',
    assetsDir: 'static',
    configureWebpack: {
        entry: './src/main.ts'
    },
    // chain webpack should be configured according to webpack.config.js
    chainWebpack: config => {
        config.module
            .rule('pre')
            .enforce('pre')
            .test(/\.(js|vue|ts)$/)
            .exclude
            .add(/node_modules/)
            .end()
            .use('eslint-loader')
            .loader('eslint-loader')
        config.module
            .rule('vue')
            .test(/\.vue$/)
            .use('vue-loader')
            .loader('vue-loader')
            .options({
                loaders: {
                    js: 'babel-loader',
                    ts: 'ts-loader',
                    scss: 'sass-loader'
                },
                esModule: true
            })
        config.module
            .rule('js')
            .test(/\.js$/)
            .exclude
            .add(/node_modules|vue\/src/)
            .end()
            .use('babel-loader')
            .loader('babel-loader')
        config.module
            .rule('tsx')
            .test(/\.tsx?$/)
            .use('ts-loader')
            .loader('ts-loader')
            .options({
                configFile: 'tsconfig.json',
                appendTsSuffixTo: [/\.vue$/]
            })
        config.module
            .rule('css')
            .test(/\.css$/)
            .use(
                'vue-style-loader',
                {
                    loader: 'css-loader',
                    options: {importLoaders: 1}
                },
                'postcss-loader'
            )
            .loader(
                'vue-style-loader',
                {
                    loader: 'css-loader',
                    options: {importLoaders: 1}
                },
                'postcss-loader'
            )
        config.module
            .rule('scss')
            .test(/\.scss$/)
            .use(
                'vue-style-loader',
                'css-loader',
                'sass-loader'
            )
            .loader(
                'vue-style-loader',
                'css-loader',
                'sass-loader'
            )
        config.resolve.extensions.prepend('.vue').prepend('.js').prepend('.tsx').prepend('.ts').prepend('.css').prepend('.scss')
    }
}
