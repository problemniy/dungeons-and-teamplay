// webpack.config.js
const path = require('path')
const VueLoaderPlugin = require('vue-loader/lib/plugin')

// keep in mind that all changes should also be implemented in vue.config.js
module.exports = {
    entry: './src/main.ts',
    output: {
        path: path.resolve(__dirname, './build/dist'),
        publicPath: '/build/dist/',
        filename: 'main.js'
    },
    module: {
        rules: [
            {
                enforce: 'pre',
                test: /\.(js|vue|ts)$/,
                loader: 'eslint-loader',
                exclude: /node_modules/
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    loaders: {
                        js: 'babel-loader',
                        ts: 'ts-loader',
                        scss: 'sass-loader'
                    },
                    esModule: true
                }
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules|vue\/src/
            },
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                options: {
                    configFile: 'tsconfig.json',
                    appendTsSuffixTo: [/\.vue$/]
                }
            },
            {
                test: /\.css$/,
                use: [
                    'vue-style-loader',
                    {
                        loader: 'css-loader',
                        options: {importLoaders: 1}
                    },
                    'postcss-loader'
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    'vue-style-loader',
                    'css-loader',
                    'sass-loader'
                ]
            }
        ]
    },
    resolve: {
        extensions: ['.vue', '.js', '.tsx', '.ts', '.css', 'scss']
    },
    plugins: [
        new VueLoaderPlugin()
    ]
};
