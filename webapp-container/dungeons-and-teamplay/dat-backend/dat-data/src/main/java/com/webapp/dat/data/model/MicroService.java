package com.webapp.dat.data.model;

import com.netflix.appinfo.InstanceInfo;

public abstract class MicroService {

    private InstanceInfo serviceInfo;

    public InstanceInfo getServiceInfo() {
        return serviceInfo;
    }

    public void setServiceInfo(InstanceInfo serviceInfo) {
        this.serviceInfo = serviceInfo;
    }

}
