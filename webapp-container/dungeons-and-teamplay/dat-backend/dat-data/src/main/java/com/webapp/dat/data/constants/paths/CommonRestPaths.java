package com.webapp.dat.data.constants.paths;

public final class CommonRestPaths {

    public static final String WBE_MAPPING = "/wbe/mapping";

    private CommonRestPaths() {
    }
}
