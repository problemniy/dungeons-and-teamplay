package com.webapp.dat.data.model.player;

import java.time.Instant;

public class VerificationToken {

    private String value;
    private Instant expiryDate;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Instant getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Instant expiryDate) {
        this.expiryDate = expiryDate;
    }
}
