package com.webapp.dat.data.constants.events;

public final class WebBusinessActionProperties {

    public static final String LOGIN = "login";
    public static final String TOKEN = "token";
    public static final String USER = "user";

    private WebBusinessActionProperties() {
    }
}
