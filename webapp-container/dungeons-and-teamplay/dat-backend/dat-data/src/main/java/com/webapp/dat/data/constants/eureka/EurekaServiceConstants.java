package com.webapp.dat.data.constants.eureka;

public final class EurekaServiceConstants {

    public static final String AUTHENTICATION_SERVICE_TAG = "dat-authentication-service";
    public static final String LOCALIZATION_SERVICE_TAG = "dat-localization-service";
    public static final String MESSENGER_SERVICE_TAG = "dat-messenger-service";

    private EurekaServiceConstants() {
    }
}
