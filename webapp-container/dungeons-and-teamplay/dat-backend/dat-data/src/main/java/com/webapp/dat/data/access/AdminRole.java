package com.webapp.dat.data.access;

import com.webapp.dat.data.access.in.IAuthority;
import com.webapp.dat.data.common.utils.AuthorityDataUtils;
import com.webapp.dat.data.constants.AuthorityConstants;

public class AdminRole implements IAuthority {

    @Override
    public String getAuthority() {
        return AuthorityDataUtils.asSpringRole(AuthorityConstants.AUTH_ADMIN);
    }
}
