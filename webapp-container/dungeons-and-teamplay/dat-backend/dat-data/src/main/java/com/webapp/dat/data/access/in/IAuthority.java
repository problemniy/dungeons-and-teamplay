package com.webapp.dat.data.access.in;

import org.springframework.security.core.GrantedAuthority;

public interface IAuthority extends GrantedAuthority {

}
