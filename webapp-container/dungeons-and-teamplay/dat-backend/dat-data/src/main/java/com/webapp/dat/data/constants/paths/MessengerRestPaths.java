package com.webapp.dat.data.constants.paths;

public final class MessengerRestPaths {

    public static String EMAIL_CONFIRM = "/confirm";

    private MessengerRestPaths() {
    }
}
