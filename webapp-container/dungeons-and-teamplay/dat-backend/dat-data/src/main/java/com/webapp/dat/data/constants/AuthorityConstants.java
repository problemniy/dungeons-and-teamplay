package com.webapp.dat.data.constants;

public final class AuthorityConstants {

    public static final String ROLE_PREFIX = "ROLE_";
    public static final String AUTH_ADMIN = "ADMIN";
    public static final String AUTH_SYSTEM = "SYSTEM";
    public static final String AUTH_USER = "USER";

    private AuthorityConstants() {

    }

}
