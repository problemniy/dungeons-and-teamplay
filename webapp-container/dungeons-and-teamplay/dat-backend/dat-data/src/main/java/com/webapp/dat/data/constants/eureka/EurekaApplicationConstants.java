package com.webapp.dat.data.constants.eureka;

public class EurekaApplicationConstants {

    public static final String AUTHENTICATION_APP_NAME = "dat-authentication";
    public static final String LOCALIZATION_APP_NAME = "dat-localization";
    public static final String MESSENGER_APP_NAME = "dat-messenger";

    private EurekaApplicationConstants() {
    }
}
