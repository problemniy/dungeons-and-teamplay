package com.webapp.dat.data.access;

import com.webapp.dat.data.constants.AuthorityConstants;
import com.webapp.dat.data.access.in.IAuthority;
import com.webapp.dat.data.common.utils.AuthorityDataUtils;

public class SystemRole implements IAuthority {

    @Override
    public String getAuthority() {
        return AuthorityDataUtils.asSpringRole(AuthorityConstants.AUTH_SYSTEM);
    }
}
