package com.webapp.dat.data.constants.events;

public final class WebBusinessActionNames {

    private WebBusinessActionNames() {
    }

    public static final class GetActions {

        public static final String CHECK_USER_EXISTS = "CHECK_USER_EXISTS";
        public static final String REGISTRATION_CONFIRM = "REGISTRATION_CONFIRM";

        private GetActions() {
        }
    }

    public static final class PostActions {

        public static final String REGISTER_USER = "REGISTER_USER";

        private PostActions() {
        }
    }

    public static final class PutActions {

        public static final String RECREATE_USER_TOKEN = "RECREATE_USER_TOKEN";

        private PutActions() {
        }
    }

    public static final class HeadActions {

        private HeadActions() {
        }
    }
}
