package com.webapp.dat.data.constants.paths;

public final class AuthenticationRestPaths {

    public static final String CREATE_USER = "/createUser";
    public static final String RECREATE_TOKEN = "/recreateToken";
    public static final String UPDATE_USER = "/updateUser";
    public static final String GET_USER_BY_NAME = "/getUserByName";
    public static final String GET_USER_BY_TOKEN = "/getUserByToken";

    private AuthenticationRestPaths() {
    }
}
