package com.webapp.dat.data.common.json.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.webapp.dat.data.common.utils.AuthorityDataUtils;
import org.springframework.security.core.GrantedAuthority;

import java.io.IOException;
import java.util.Collection;

public class AuthoritySerializer extends JsonSerializer<Collection<GrantedAuthority>> {

    @Override
    public void serialize(Collection<GrantedAuthority> value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        String[] roles = AuthorityDataUtils.asStringRolesArray(value, false);

        gen.writeStartArray();
        for (String role : roles) {
            gen.writeString(role);
        }
        gen.writeEndArray();
    }
}
