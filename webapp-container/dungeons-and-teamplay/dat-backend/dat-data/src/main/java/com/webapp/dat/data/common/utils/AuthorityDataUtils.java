package com.webapp.dat.data.common.utils;

import com.webapp.dat.data.access.AdminRole;
import com.webapp.dat.data.access.SystemRole;
import com.webapp.dat.data.access.UserRole;
import com.webapp.dat.data.access.in.IAuthority;
import com.webapp.dat.data.constants.AuthorityConstants;
import org.springframework.lang.Nullable;
import org.springframework.security.core.GrantedAuthority;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

public final class AuthorityDataUtils {

    private AuthorityDataUtils() {
    }

    public static List<GrantedAuthority> parseRoles(List<String> roles) {
        return parseRoles(roles.toArray(String[]::new));
    }

    public static List<GrantedAuthority> parseRoles(String[] roles) {
        return Arrays.stream(roles).map(AuthorityDataUtils::getAuthority).filter(Objects::nonNull).collect(Collectors.toList());
    }

    @Nullable
    public static IAuthority getAuthority(String authorityName) {
        IAuthority result = null;
        String role = authorityName.toUpperCase();
        switch (role) {
            case AuthorityConstants.AUTH_ADMIN:
                result = new AdminRole();
                break;
            case AuthorityConstants.AUTH_SYSTEM:
                result = new SystemRole();
                break;
            case AuthorityConstants.AUTH_USER:
                result = new UserRole();
                break;
        }
        return result;
    }

    public static String asSpringRole(String roleName) {
        String result = roleName;
        if (!result.startsWith(AuthorityConstants.ROLE_PREFIX)) {
            result = AuthorityConstants.ROLE_PREFIX.concat(result);
        }
        return result;
    }

    public static String asRole(String roleName) {
        return roleName.replace(AuthorityConstants.ROLE_PREFIX, "");
    }

    public static String[] asStringRolesArray(Collection<? extends GrantedAuthority> roles, boolean asSpringRole) {
        return roles.stream().map(
                (Function<GrantedAuthority, String>) grantedAuthority -> {
                    String role = grantedAuthority.getAuthority();
                    return asSpringRole ? asSpringRole(role) : asRole(role);
                }
        ).toArray(String[]::new);
    }
}
