package com.webapp.dat.data.enumeration;

import com.webapp.dat.data.constants.eureka.EurekaApplicationConstants;
import com.webapp.dat.data.constants.eureka.EurekaServiceConstants;

public enum MicroServiceSpecification {

    AUTHENTICATION(EurekaApplicationConstants.AUTHENTICATION_APP_NAME, EurekaServiceConstants.AUTHENTICATION_SERVICE_TAG),
    LOCALIZATION(EurekaApplicationConstants.LOCALIZATION_APP_NAME, EurekaServiceConstants.LOCALIZATION_SERVICE_TAG),
    MESSENGER(EurekaApplicationConstants.MESSENGER_APP_NAME, EurekaServiceConstants.MESSENGER_SERVICE_TAG);

    private final String appName;
    private final String serviceName;

    MicroServiceSpecification(String appName, String serviceName) {
        this.appName = appName;
        this.serviceName = serviceName;
    }

    public String getAppName() {
        return appName;
    }

    public String getServiceName() {
        return serviceName;
    }
}
