package com.webapp.dat.data.model.player;

import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class DefaultUser extends User {

    private final String servicePassword;

    public DefaultUser(String username, String password, Collection<? extends GrantedAuthority> authorities, String servicePassword) {
        super(username, password, authorities);
        this.servicePassword = servicePassword;
    }

    public DefaultUser(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked,
                       Collection<? extends GrantedAuthority> authorities, String servicePassword) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
        this.servicePassword = servicePassword;
    }

    @Override
    public void eraseCredentials() {
        // we shouldn't clear password for default user
    }

    public String getServicePassword() {
        return servicePassword;
    }
}
