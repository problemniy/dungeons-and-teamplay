package com.webapp.dat.gateway.component.service;

import org.springframework.http.HttpHeaders;

import javax.servlet.http.HttpServletRequest;

public interface IRestSecurityService {

    HttpHeaders getSecuredHeaders();

    HttpHeaders getCsrfSecuredHeaders(HttpServletRequest request);

    HttpHeaders clearCsrfTokens(HttpHeaders headers);
}
