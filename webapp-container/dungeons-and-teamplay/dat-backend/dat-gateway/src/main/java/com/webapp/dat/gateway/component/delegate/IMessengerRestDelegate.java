package com.webapp.dat.gateway.component.delegate;

import org.springframework.http.HttpHeaders;

public interface IMessengerRestDelegate {

    HttpHeaders sendConfirmationLink(String userName, String link, String token, String locale);
}
