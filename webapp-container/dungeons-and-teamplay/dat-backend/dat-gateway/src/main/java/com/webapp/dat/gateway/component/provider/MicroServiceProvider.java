package com.webapp.dat.gateway.component.provider;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.shared.Application;
import com.netflix.discovery.shared.Applications;
import com.netflix.eureka.registry.PeerAwareInstanceRegistry;
import com.webapp.dat.data.enumeration.MicroServiceSpecification;
import com.webapp.dat.data.model.MicroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;

@Component
public class MicroServiceProvider {

    private final PeerAwareInstanceRegistry registry;

    @Autowired
    public MicroServiceProvider(PeerAwareInstanceRegistry registry) {
        this.registry = registry;
    }

    @Nullable
    public <MS extends MicroService> MS getMicroService(Class<MS> serviceClass, MicroServiceSpecification microServiceSpecification) {
        Applications applications = registry.getApplications();
        Application application = applications.getRegisteredApplications(microServiceSpecification.getAppName());
        MS serviceObject = null;
        InstanceInfo serviceInfo = null;
        if (application != null) {
            serviceInfo = application.getByInstanceId(microServiceSpecification.getServiceName());
        }
        if (serviceInfo != null) {
            try {
                serviceObject = serviceClass.getConstructor(new Class[]{}).newInstance();
                serviceObject.setServiceInfo(serviceInfo);
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                throw new IllegalStateException(serviceClass.toString() + " should have default constructor!");
            }
        }
        return serviceObject;
    }
}
