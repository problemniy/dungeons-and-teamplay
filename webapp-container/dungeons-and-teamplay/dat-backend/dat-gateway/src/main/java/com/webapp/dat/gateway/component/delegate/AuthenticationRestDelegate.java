package com.webapp.dat.gateway.component.delegate;

import com.webapp.dat.data.constants.paths.AuthenticationRestPaths;
import com.webapp.dat.data.constants.paths.CommonRestPaths;
import com.webapp.dat.data.enumeration.MicroServiceSpecification;
import com.webapp.dat.data.model.player.User;
import com.webapp.dat.gateway.component.delegate.abs.ARestDelegate;
import com.webapp.dat.gateway.component.provider.MicroServiceProvider;
import com.webapp.dat.gateway.component.service.IRestSecurityService;
import com.webapp.dat.gateway.microservice.AuthenticationMicroService;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;

public class AuthenticationRestDelegate extends ARestDelegate implements IAuthenticationRestDelegate {

    public AuthenticationRestDelegate(RestTemplate restTemplate, MicroServiceProvider microServiceProvider, IRestSecurityService restSecurityService) {
        super(restTemplate, microServiceProvider, restSecurityService);
    }

    @Nullable
    @Override
    public ResponseEntity<User> createUser(User user, HttpHeaders headers) {
        AuthenticationMicroService authenticationService = microServiceProvider.getMicroService(AuthenticationMicroService.class, MicroServiceSpecification.AUTHENTICATION);
        if (authenticationService == null) {
            return null;
        }
        URI uri = UriComponentsBuilder.fromHttpUrl(authenticationService.getServiceInfo().getHomePageUrl() + AuthenticationRestPaths.CREATE_USER)
                .build().toUri();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<User> entity = getSecuredEntity(user, headers);
        return restTemplate.exchange(uri, HttpMethod.POST, entity, User.class);
    }

    @Nullable
    @Override
    public ResponseEntity<User> recreateAccessToken(String previousToken, HttpHeaders headers, HttpServletRequest request) {
        AuthenticationMicroService authenticationService = microServiceProvider.getMicroService(AuthenticationMicroService.class, MicroServiceSpecification.AUTHENTICATION);
        if (authenticationService == null) {
            return null;
        }
        URI uri = UriComponentsBuilder.fromHttpUrl(authenticationService.getServiceInfo().getHomePageUrl() + AuthenticationRestPaths.RECREATE_TOKEN)
                .queryParam("previousToken", previousToken)
                .build().toUri();
        return restTemplate.exchange(uri, HttpMethod.PUT, getCsrfSecuredEntity(request), User.class);
    }

    @Nullable
    @Override
    public ResponseEntity<User> updateUser(User user, HttpHeaders headers) {
        AuthenticationMicroService authenticationService = microServiceProvider.getMicroService(AuthenticationMicroService.class, MicroServiceSpecification.AUTHENTICATION);
        if (authenticationService == null) {
            return null;
        }
        headers.setContentType(MediaType.APPLICATION_JSON);
        return updateUser(authenticationService, headers, getSecuredEntity(user, headers));
    }

    @Override
    public ResponseEntity<User> updateUnauthorizedUser(User user, HttpHeaders headers, HttpServletRequest request) {
        AuthenticationMicroService authenticationService = microServiceProvider.getMicroService(AuthenticationMicroService.class, MicroServiceSpecification.AUTHENTICATION);
        if (authenticationService == null) {
            return null;
        }
        headers.setContentType(MediaType.APPLICATION_JSON);
        return updateUser(authenticationService, headers, getCsrfSecuredEntity(user, headers, request));
    }

    private ResponseEntity<User> updateUser(AuthenticationMicroService authenticationService, HttpHeaders headers, HttpEntity<User> entity) {
        URI uri = UriComponentsBuilder.fromHttpUrl(authenticationService.getServiceInfo().getHomePageUrl() + AuthenticationRestPaths.UPDATE_USER)
                .build().toUri();
        return restTemplate.exchange(uri, HttpMethod.PUT, entity, User.class);
    }

    @Nullable
    @Override
    public ResponseEntity<User> getUserByName(String userName) {
        AuthenticationMicroService authenticationService = microServiceProvider.getMicroService(AuthenticationMicroService.class, MicroServiceSpecification.AUTHENTICATION);
        if (authenticationService == null) {
            return null;
        }
        URI uri = UriComponentsBuilder.fromHttpUrl(authenticationService.getServiceInfo().getHomePageUrl() + AuthenticationRestPaths.GET_USER_BY_NAME)
                .queryParam("name", userName)
                .build().toUri();
        return restTemplate.exchange(uri, HttpMethod.GET, getSecuredEntity(), User.class);
    }

    @Nullable
    @Override
    public ResponseEntity<User> getUserByToken(String token) {
        AuthenticationMicroService authenticationService = microServiceProvider.getMicroService(AuthenticationMicroService.class, MicroServiceSpecification.AUTHENTICATION);
        if (authenticationService == null) {
            return null;
        }
        URI uri = UriComponentsBuilder.fromHttpUrl(authenticationService.getServiceInfo().getHomePageUrl() + AuthenticationRestPaths.GET_USER_BY_TOKEN)
                .queryParam("token", token)
                .build().toUri();
        return restTemplate.exchange(uri, HttpMethod.GET, getSecuredEntity(), User.class);
    }

    @Nullable
    @Override
    public String getWbeMapping() {
        AuthenticationMicroService authenticationService = microServiceProvider.getMicroService(AuthenticationMicroService.class, MicroServiceSpecification.AUTHENTICATION);
        if (authenticationService == null) {
            return null;
        }
        URI uri = UriComponentsBuilder.fromHttpUrl(authenticationService.getServiceInfo().getHomePageUrl() + CommonRestPaths.WBE_MAPPING).build().toUri();
        return restTemplate.exchange(uri, HttpMethod.GET, getSecuredEntity(), String.class).getBody();
    }
}
