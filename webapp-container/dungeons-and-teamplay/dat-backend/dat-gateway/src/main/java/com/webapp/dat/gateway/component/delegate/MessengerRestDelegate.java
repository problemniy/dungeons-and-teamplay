package com.webapp.dat.gateway.component.delegate;

import com.webapp.dat.data.constants.paths.MessengerRestPaths;
import com.webapp.dat.data.enumeration.MicroServiceSpecification;
import com.webapp.dat.gateway.component.delegate.abs.ARestDelegate;
import com.webapp.dat.gateway.component.provider.MicroServiceProvider;
import com.webapp.dat.gateway.component.service.IRestSecurityService;
import com.webapp.dat.gateway.microservice.MessengerMicroService;
import org.springframework.http.HttpHeaders;
import org.springframework.lang.Nullable;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

public class MessengerRestDelegate extends ARestDelegate implements IMessengerRestDelegate {

    public MessengerRestDelegate(RestTemplate restTemplate, MicroServiceProvider microServiceProvider, IRestSecurityService restSecurityService) {
        super(restTemplate, microServiceProvider, restSecurityService);
    }

    @Nullable
    @Override
    public HttpHeaders sendConfirmationLink(String userName, String email, String link, String locale) {
        MessengerMicroService messengerMicroService = microServiceProvider.getMicroService(MessengerMicroService.class, MicroServiceSpecification.MESSENGER);
        if (messengerMicroService == null) {
            return null;
        }
        URI uri = UriComponentsBuilder.fromHttpUrl(messengerMicroService.getServiceInfo().getHomePageUrl() + MessengerRestPaths.EMAIL_CONFIRM)
                .queryParam("userName", userName)
                .queryParam("email", email)
                .queryParam("link", link)
                .queryParam("locale", locale)
                .build().toUri();
        return restTemplate.headForHeaders(uri);
    }
}
