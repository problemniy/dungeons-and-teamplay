package com.webapp.dat.gateway.component.delegate;

import com.webapp.dat.data.model.player.User;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;

import javax.servlet.http.HttpServletRequest;

public interface IAuthenticationRestDelegate {

    @Nullable
    ResponseEntity<User> createUser(User user, HttpHeaders headers);

    @Nullable
    ResponseEntity<User> recreateAccessToken(String previousToken, HttpHeaders headers, HttpServletRequest request);

    @Nullable
    ResponseEntity<User> updateUser(User user, HttpHeaders headers);

    @Nullable
    ResponseEntity<User> updateUnauthorizedUser(User user, HttpHeaders headers, HttpServletRequest request);

    @Nullable
    ResponseEntity<User> getUserByName(String userName);

    @Nullable
    ResponseEntity<User> getUserByToken(String token);

    @Nullable
    String getWbeMapping();
}
