package com.webapp.dat.gateway.component.delegate.abs;

import com.webapp.dat.gateway.component.provider.MicroServiceProvider;
import com.webapp.dat.gateway.component.service.IRestSecurityService;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;

public abstract class ARestDelegate {

    protected final RestTemplate restTemplate;
    protected final MicroServiceProvider microServiceProvider;
    protected final IRestSecurityService restSecurityService;

    public ARestDelegate(RestTemplate restTemplate, MicroServiceProvider microServiceProvider, IRestSecurityService restSecurityService) {
        this.restTemplate = restTemplate;
        this.microServiceProvider = microServiceProvider;
        this.restSecurityService = restSecurityService;
    }

    protected <T> HttpEntity<T> getSecuredEntity() {
        HttpHeaders headers = restSecurityService.getSecuredHeaders();
        return new HttpEntity<>(headers);
    }

    protected <T> HttpEntity<T> getCsrfSecuredEntity(HttpServletRequest request) {
        HttpHeaders headers = restSecurityService.getCsrfSecuredHeaders(request);
        return new HttpEntity<>(headers);
    }

    protected <T> HttpEntity<T> getSecuredEntity(HttpHeaders headers) {
        HttpHeaders actualHeaders = restSecurityService.getSecuredHeaders();
        actualHeaders.addAll(headers);
        return new HttpEntity<>(actualHeaders);
    }

    protected <T> HttpEntity<T> getCsrfSecuredEntity(HttpHeaders headers, HttpServletRequest request) {
        HttpHeaders actualHeaders = restSecurityService.getCsrfSecuredHeaders(request);
        actualHeaders.addAll(restSecurityService.clearCsrfTokens(headers));
        return new HttpEntity<>(actualHeaders);
    }

    protected <T> HttpEntity<T> getSecuredEntity(T body) {
        HttpHeaders headers = restSecurityService.getSecuredHeaders();
        return new HttpEntity<>(body, headers);
    }

    protected <T> HttpEntity<T> getCsrfSecuredEntity(T body, HttpServletRequest request) {
        HttpHeaders headers = restSecurityService.getCsrfSecuredHeaders(request);
        return new HttpEntity<>(body, headers);
    }

    protected <T> HttpEntity<T> getSecuredEntity(T body, HttpHeaders headers) {
        HttpHeaders actualHeaders = restSecurityService.getSecuredHeaders();
        actualHeaders.addAll(headers);
        return new HttpEntity<>(body, actualHeaders);
    }

    protected <T> HttpEntity<T> getCsrfSecuredEntity(T body, HttpHeaders headers, HttpServletRequest request) {
        HttpHeaders actualHeaders = restSecurityService.getCsrfSecuredHeaders(request);
        actualHeaders.addAll(restSecurityService.clearCsrfTokens(headers));
        return new HttpEntity<>(body, actualHeaders);
    }
}
