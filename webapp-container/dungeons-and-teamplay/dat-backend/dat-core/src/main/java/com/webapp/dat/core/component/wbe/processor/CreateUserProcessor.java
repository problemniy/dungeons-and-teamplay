package com.webapp.dat.core.component.wbe.processor;

import com.tools.wbe.data.business.delegate.IWebBusinessProcess;
import com.tools.wbe.data.business.processor.IPostActionProcessor;
import com.tools.wbe.data.context.PostRequestContext;
import com.tools.wbe.data.context.PostResponseContext;
import com.webapp.dat.data.model.player.User;
import com.webapp.dat.gateway.component.delegate.IAuthenticationRestDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component("createUserProcessor")
public class CreateUserProcessor implements IPostActionProcessor {

    private final IAuthenticationRestDelegate authenticationRestDelegate;

    @Autowired
    public CreateUserProcessor(@Qualifier("securedAuthenticationRestDelegate") IAuthenticationRestDelegate authenticationRestDelegate) {
        this.authenticationRestDelegate = authenticationRestDelegate;
    }

    @Override
    public void execute(PostRequestContext requestContext, PostResponseContext responseContext, IWebBusinessProcess businessProcess) {
        User user = requestContext.getBody(User.class);
        if (user == null) {
            return;
        }
        ResponseEntity<User> response = authenticationRestDelegate.createUser(user, requestContext.getHeaders());
        if (response != null) {
            responseContext.setBody(response.getBody());
        }
    }
}
