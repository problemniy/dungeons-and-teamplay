package com.webapp.dat.core.controller.rest;

import com.webapp.dat.core.component.service.IWbeBusinessManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/wbe")
public class WbeDelegateRestController {

    private final IWbeBusinessManagementService wbeBusinessManagementService;

    @Autowired
    public WbeDelegateRestController(IWbeBusinessManagementService wbeBusinessManagementService) {
        this.wbeBusinessManagementService = wbeBusinessManagementService;
    }

    @GetMapping(value = "/auth/mapping", produces = MediaType.TEXT_HTML_VALUE)
    public String getAuthenticationWbeMapping() {
        return wbeBusinessManagementService.getAuthenticationWbeMapping();
    }
}
