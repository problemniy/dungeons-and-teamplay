package com.webapp.dat.core.controller;

import com.webapp.dat.core.component.service.IAuthBusinessManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping(value = "/auth")
public class AuthenticationController {

    private final IAuthBusinessManagementService businessManagementService;

    @Autowired
    public AuthenticationController(IAuthBusinessManagementService businessManagementService) {
        this.businessManagementService = businessManagementService;
    }

    @GetMapping(value = "/confirm")
    public String confirm(@RequestParam String token, @RequestHeader HttpHeaders headers, HttpServletRequest request, HttpServletResponse response) {
        HttpStatus status = businessManagementService.registrationConfirm(token, headers, request, response);
        response.setStatus(status.value());
        return "forward:/";
    }
}
