package com.webapp.dat.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@SpringBootApplication(
        exclude = {
                org.springframework.boot.autoconfigure.gson.GsonAutoConfiguration.class
        })
@ComponentScan({
        "com.webapp.dat.core",
        "com.webapp.dat.gateway",
        "com.webapp.dat.security"
})
@ImportResource("classpath:/context/wbe-processors.config.xml")
@EnableEurekaServer
@PropertySources({
        @PropertySource("classpath:security.properties"),
        @PropertySource("classpath:server.properties")
})
public class DATApp {
    public static void main(String[] args) {
        SpringApplication.run(DATApp.class, args);
    }
}
