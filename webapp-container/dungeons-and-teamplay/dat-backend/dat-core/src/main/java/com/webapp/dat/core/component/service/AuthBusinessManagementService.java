package com.webapp.dat.core.component.service;

import com.tools.wbe.core.component.service.WebBusinessEventsService;
import com.tools.wbe.data.context.GetRequestContext;
import com.tools.wbe.data.context.GetResponseContext;
import com.tools.wbe.data.context.PostRequestContext;
import com.tools.wbe.data.context.PostResponseContext;
import com.tools.wbe.data.context.PutRequestContext;
import com.tools.wbe.data.context.PutResponseContext;
import com.tools.wbe.manage.executor.GetActionExecutor;
import com.tools.wbe.manage.executor.PostActionExecutor;
import com.tools.wbe.manage.executor.PutActionExecutor;
import com.webapp.dat.data.constants.events.WebBusinessActionNames;
import com.webapp.dat.data.constants.events.WebBusinessActionProperties;
import com.webapp.dat.data.model.player.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Service
public class AuthBusinessManagementService implements IAuthBusinessManagementService {

    private final PostActionExecutor postActionExecutor;
    private final PutActionExecutor putActionExecutor;
    private final GetActionExecutor getActionExecutor;
    private final WebBusinessEventsService webBusinessEventsService;

    @Autowired
    public AuthBusinessManagementService(PostActionExecutor postActionExecutor, GetActionExecutor getActionExecutor, PutActionExecutor putActionExecutor,
                                         WebBusinessEventsService webBusinessEventsService) {
        this.postActionExecutor = postActionExecutor;
        this.putActionExecutor = putActionExecutor;
        this.getActionExecutor = getActionExecutor;
        this.webBusinessEventsService = webBusinessEventsService;
    }

    @Override
    public ResponseEntity<User> createUser(User user, HttpHeaders headers, HttpServletRequest request, HttpServletResponse response) {
        return postActionExecutor.prepare(
                () -> webBusinessEventsService.prepareRequestContext(headers, request, user, PostRequestContext.class),
                () -> webBusinessEventsService.prepareResponseContext(new HttpHeaders(), response, PostResponseContext.class))
                .addAction(WebBusinessActionNames.PostActions.REGISTER_USER)
                .go().asResponseEntity(User.class);
    }

    @Override
    public ResponseEntity<Boolean> checkUserExists(String login, HttpHeaders headers, HttpServletRequest request, HttpServletResponse response) {
        return getActionExecutor.prepare(
                () -> webBusinessEventsService.prepareRequestContext(headers, request, GetRequestContext.class),
                () -> webBusinessEventsService.prepareResponseContext(new HttpHeaders(), response, GetResponseContext.class))
                .addAction(WebBusinessActionNames.GetActions.CHECK_USER_EXISTS)
                .addProperty(WebBusinessActionProperties.LOGIN, login)
                .go().asResponseEntity(Boolean.class);
    }

    @Override
    public HttpStatus recreateToken(String previousToken, HttpHeaders headers, HttpServletRequest request, HttpServletResponse response) {
        return putActionExecutor.prepare(
                () -> webBusinessEventsService.prepareRequestContext(headers, request, PutRequestContext.class),
                () -> webBusinessEventsService.prepareResponseContext(new HttpHeaders(), response, null, PutResponseContext.class))
                .addAction(WebBusinessActionNames.PutActions.RECREATE_USER_TOKEN)
                .addProperty(WebBusinessActionProperties.TOKEN, previousToken)
                .go().asResponseContext().getHttpStatus();
    }

    @Override
    public HttpStatus registrationConfirm(String token, HttpHeaders headers, HttpServletRequest request, HttpServletResponse response) {
        return getActionExecutor.prepare(
                () -> webBusinessEventsService.prepareRequestContext(headers, request, GetRequestContext.class),
                () -> webBusinessEventsService.prepareResponseContext(new HttpHeaders(), response, GetResponseContext.class))
                .addAction(WebBusinessActionNames.GetActions.REGISTRATION_CONFIRM)
                .addProperty(WebBusinessActionProperties.TOKEN, token)
                .go().asResponseContext().getHttpStatus();
    }
}
