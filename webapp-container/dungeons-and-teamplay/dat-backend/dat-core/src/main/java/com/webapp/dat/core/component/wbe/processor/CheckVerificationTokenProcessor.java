package com.webapp.dat.core.component.wbe.processor;

import com.tools.wbe.data.business.delegate.IWebBusinessProcess;
import com.tools.wbe.data.business.processor.IGetActionProcessor;
import com.tools.wbe.data.common.spec.EWebEventStrategy;
import com.tools.wbe.data.context.GetRequestContext;
import com.tools.wbe.data.context.GetResponseContext;
import com.webapp.dat.data.constants.events.WebBusinessActionProperties;
import com.webapp.dat.data.model.player.User;
import com.webapp.dat.gateway.component.delegate.IAuthenticationRestDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.Instant;

@Component("checkVerificationTokenProcessor")
public class CheckVerificationTokenProcessor implements IGetActionProcessor {

    private final IAuthenticationRestDelegate authenticationRestDelegate;

    @Autowired
    public CheckVerificationTokenProcessor(@Qualifier("securedAuthenticationRestDelegate") IAuthenticationRestDelegate authenticationRestDelegate) {
        this.authenticationRestDelegate = authenticationRestDelegate;
    }

    @Override
    public void execute(GetRequestContext requestContext, GetResponseContext responseContext, IWebBusinessProcess businessProcess) {
        String token = businessProcess.properties().getProperty(WebBusinessActionProperties.TOKEN, String.class);
        if (token == null) {
            responseContext.setBody(Boolean.FALSE);
        }
        ResponseEntity<User> response = authenticationRestDelegate.getUserByToken(token);
        if (response != null && response.getBody() != null) {
            User user = response.getBody();
            if (user.isEnabled()) {
                responseContext.setHttpStatus(HttpStatus.CONFLICT);
                businessProcess.sendStrategy(EWebEventStrategy.ERROR);
                return;
            }
            Instant expiredDate = user.getVerificationToken().getExpiryDate();
            if (Instant.now().isAfter(expiredDate)) {
                try {
                    responseContext.getResponse().sendError(HttpStatus.NOT_ACCEPTABLE.value(), "Token has been expired");
                    responseContext.setHttpStatus(HttpStatus.NOT_ACCEPTABLE);
                    businessProcess.sendStrategy(EWebEventStrategy.ERROR);
                } catch (IOException ignored) {
                }
            } else {
                user.getAccessData().setEnabled(true);
                businessProcess.properties().addProperty(WebBusinessActionProperties.USER, user);
            }
        } else {
            responseContext.setHttpStatus(HttpStatus.NOT_FOUND);
            businessProcess.sendStrategy(EWebEventStrategy.ERROR);
        }
    }
}
