package com.webapp.dat.core.controller.rest;

import com.webapp.dat.core.component.service.IAuthBusinessManagementService;
import com.webapp.dat.data.model.player.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/auth")
public class AuthenticationRestController {

    private final IAuthBusinessManagementService businessManagementService;

    @Autowired
    public AuthenticationRestController(IAuthBusinessManagementService businessManagementService) {
        this.businessManagementService = businessManagementService;
    }

    @PostMapping(path = "/reg", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> registerUser(@RequestBody User user, @RequestHeader HttpHeaders headers,
                                             HttpServletRequest request, HttpServletResponse response) {
        return businessManagementService.createUser(user, headers, request, response);
    }

    @PutMapping(path = "/recreateToken", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public HttpStatus recreateToken(@RequestBody String previousToken, @RequestHeader HttpHeaders headers,
                                    HttpServletRequest request, HttpServletResponse response) {
        return businessManagementService.recreateToken(previousToken, headers, request, response);
    }

    @GetMapping(value = "/userExists")
    public boolean userExists(@RequestParam String login, @RequestHeader HttpHeaders headers,
                              HttpServletRequest request, HttpServletResponse response) {
        Boolean result = businessManagementService.checkUserExists(login, headers, request, response).getBody();
        return result != null ? result : true;
    }
}
