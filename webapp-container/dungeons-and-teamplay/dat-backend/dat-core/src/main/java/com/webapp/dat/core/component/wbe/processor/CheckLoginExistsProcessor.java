package com.webapp.dat.core.component.wbe.processor;

import com.tools.wbe.data.business.delegate.IWebBusinessProcess;
import com.tools.wbe.data.business.processor.IGetActionProcessor;
import com.tools.wbe.data.context.GetRequestContext;
import com.tools.wbe.data.context.GetResponseContext;
import com.webapp.dat.data.constants.events.WebBusinessActionProperties;
import com.webapp.dat.data.model.player.User;
import com.webapp.dat.gateway.component.delegate.IAuthenticationRestDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component("checkLoginExistsProcessor")
public class CheckLoginExistsProcessor implements IGetActionProcessor {

    private final IAuthenticationRestDelegate authenticationRestDelegate;

    @Autowired
    public CheckLoginExistsProcessor(@Qualifier("securedAuthenticationRestDelegate") IAuthenticationRestDelegate authenticationRestDelegate) {
        this.authenticationRestDelegate = authenticationRestDelegate;
    }

    @Override
    public void execute(GetRequestContext requestContext, GetResponseContext responseContext, IWebBusinessProcess businessProcess) {
        String login = businessProcess.properties().getProperty(WebBusinessActionProperties.LOGIN, String.class);
        if (login != null) {
            ResponseEntity<User> response = authenticationRestDelegate.getUserByName(login);
            responseContext.setBody(response == null || response.getBody() != null);
        }
    }
}
