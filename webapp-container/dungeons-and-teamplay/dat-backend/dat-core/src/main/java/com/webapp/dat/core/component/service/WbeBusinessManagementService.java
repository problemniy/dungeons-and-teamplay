package com.webapp.dat.core.component.service;

import com.webapp.dat.gateway.component.delegate.IAuthenticationRestDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class WbeBusinessManagementService implements IWbeBusinessManagementService {

    private final IAuthenticationRestDelegate authenticationRestDelegate;

    @Autowired
    public WbeBusinessManagementService(@Qualifier("securedAuthenticationRestDelegate") IAuthenticationRestDelegate authenticationRestDelegate) {
        this.authenticationRestDelegate = authenticationRestDelegate;
    }

    @Override
    public String getAuthenticationWbeMapping() {
        return authenticationRestDelegate.getWbeMapping();
    }
}
