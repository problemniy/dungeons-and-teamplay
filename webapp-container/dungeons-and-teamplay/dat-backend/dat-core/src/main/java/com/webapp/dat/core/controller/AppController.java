package com.webapp.dat.core.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.webapp.dat.data.enumeration.MicroServiceSpecification;
import com.webapp.dat.gateway.component.provider.MicroServiceProvider;
import com.webapp.dat.gateway.microservice.LocalizationMicroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

//todo: implement localization controller and delete this
@RestController
@RequestMapping("/app")
public class AppController {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    private MicroServiceProvider microServiceProvider;

    @RequestMapping(path = "/locale/lang/{locale}")
    public @ResponseBody
    String locale(@PathVariable("locale") String locale) {
        JsonObject jsonObj = new JsonObject();
        LocalizationMicroService localizationMicroService = microServiceProvider.getMicroService(LocalizationMicroService.class, MicroServiceSpecification.LOCALIZATION);
        if (localizationMicroService != null) {
            URI uri = UriComponentsBuilder.fromHttpUrl(localizationMicroService.getServiceInfo().getHomePageUrl() + "/lang/ru").build().toUri();
            jsonObj = new Gson().fromJson(restTemplate.getForObject(uri, String.class), JsonObject.class);
        }
        return jsonObj.toString();
    }
}
