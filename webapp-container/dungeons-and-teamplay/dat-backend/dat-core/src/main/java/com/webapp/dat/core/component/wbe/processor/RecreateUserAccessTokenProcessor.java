package com.webapp.dat.core.component.wbe.processor;

import com.tools.wbe.data.business.delegate.IWebBusinessProcess;
import com.tools.wbe.data.business.processor.IPutActionProcessor;
import com.tools.wbe.data.context.PutRequestContext;
import com.tools.wbe.data.context.PutResponseContext;
import com.webapp.dat.data.constants.events.WebBusinessActionProperties;
import com.webapp.dat.data.model.player.User;
import com.webapp.dat.gateway.component.delegate.IAuthenticationRestDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component("recreateUserAccessTokenProcessor")
public class RecreateUserAccessTokenProcessor implements IPutActionProcessor {

    private final IAuthenticationRestDelegate authenticationRestDelegate;

    @Autowired
    public RecreateUserAccessTokenProcessor(@Qualifier("securedAuthenticationRestDelegate") IAuthenticationRestDelegate authenticationRestDelegate) {
        this.authenticationRestDelegate = authenticationRestDelegate;
    }

    @Override
    public void execute(PutRequestContext requestContext, PutResponseContext responseContext, IWebBusinessProcess businessProcess) {
        String token = businessProcess.properties().getProperty(WebBusinessActionProperties.TOKEN, String.class);
        if (token == null) {
            return;
        }
        ResponseEntity<User> response = authenticationRestDelegate.recreateAccessToken(token, new HttpHeaders(), requestContext.getRequest());
        if (response != null && response.getBody() != null) {
            responseContext.setBody(response.getBody());
        }
    }
}
