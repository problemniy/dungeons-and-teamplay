package com.webapp.dat.core.component.wbe.processor;

import com.tools.wbe.data.business.delegate.IWebBusinessProcess;
import com.tools.wbe.data.business.processor.IGetActionProcessor;
import com.tools.wbe.data.context.GetRequestContext;
import com.tools.wbe.data.context.GetResponseContext;
import com.webapp.dat.data.constants.events.WebBusinessActionProperties;
import com.webapp.dat.data.model.player.User;
import com.webapp.dat.gateway.component.delegate.IAuthenticationRestDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component("updateUnauthorizedUserProcessor")
public class UpdateUnauthorizedUserProcessor implements IGetActionProcessor {

    private final IAuthenticationRestDelegate authenticationRestDelegate;

    @Autowired
    public UpdateUnauthorizedUserProcessor(@Qualifier("securedAuthenticationRestDelegate") IAuthenticationRestDelegate authenticationRestDelegate) {
        this.authenticationRestDelegate = authenticationRestDelegate;
    }

    @Override
    public void execute(GetRequestContext requestContext, GetResponseContext responseContext, IWebBusinessProcess businessProcess) {
        User user = businessProcess.properties().getProperty(WebBusinessActionProperties.USER, User.class);
        if (user == null) {
            return;
        }
        ResponseEntity<User> responseEntity = authenticationRestDelegate.updateUnauthorizedUser(user, requestContext.getHeaders(), requestContext.getRequest());
        if (responseEntity != null && responseEntity.getBody() != null) {
            businessProcess.properties().addProperty(WebBusinessActionProperties.USER, responseEntity.getBody());
        }
    }
}
