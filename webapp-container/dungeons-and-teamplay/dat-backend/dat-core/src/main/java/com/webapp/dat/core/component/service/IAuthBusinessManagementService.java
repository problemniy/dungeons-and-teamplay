package com.webapp.dat.core.component.service;

import com.webapp.dat.data.model.player.User;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface IAuthBusinessManagementService {

    ResponseEntity<User> createUser(User user, HttpHeaders headers, HttpServletRequest request, HttpServletResponse response);

    ResponseEntity<Boolean> checkUserExists(String login, HttpHeaders headers, HttpServletRequest request, HttpServletResponse response);

    HttpStatus recreateToken(String previousToken, HttpHeaders headers, HttpServletRequest request, HttpServletResponse response);

    HttpStatus registrationConfirm(String token, HttpHeaders headers, HttpServletRequest request, HttpServletResponse response);
}
