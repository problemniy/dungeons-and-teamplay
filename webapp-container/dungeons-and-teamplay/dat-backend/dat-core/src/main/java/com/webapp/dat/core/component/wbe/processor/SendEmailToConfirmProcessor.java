package com.webapp.dat.core.component.wbe.processor;

import com.tools.wbe.data.business.delegate.IWebBusinessProcess;
import com.tools.wbe.data.business.processor.IPostActionProcessor;
import com.tools.wbe.data.business.processor.IPutActionProcessor;
import com.tools.wbe.data.context.PostRequestContext;
import com.tools.wbe.data.context.PostResponseContext;
import com.tools.wbe.data.context.PutRequestContext;
import com.tools.wbe.data.context.PutResponseContext;
import com.tools.wbe.data.context.abs.AWebRequestContext;
import com.tools.wbe.data.context.type.IWebBodyContext;
import com.webapp.dat.data.model.player.User;
import com.webapp.dat.gateway.component.delegate.IMessengerRestDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component("sendEmailToConfirmProcessor")
public class SendEmailToConfirmProcessor implements IPostActionProcessor, IPutActionProcessor {

    private final IMessengerRestDelegate messengerRestDelegate;

    @Autowired
    public SendEmailToConfirmProcessor(@Qualifier("securedMessengerRestDelegate") IMessengerRestDelegate messengerRestDelegate) {
        this.messengerRestDelegate = messengerRestDelegate;
    }

    @Override
    public void execute(PutRequestContext requestContext, PutResponseContext responseContext, IWebBusinessProcess businessProcess) {
        sendConfirmationEmail(requestContext, responseContext);
    }

    @Override
    public void execute(PostRequestContext requestContext, PostResponseContext responseContext, IWebBusinessProcess businessProcess) {
        sendConfirmationEmail(requestContext, responseContext);
    }

    private void sendConfirmationEmail(AWebRequestContext requestContext, IWebBodyContext responseContext) {
        User user = responseContext.getBody(User.class);
        String token = user != null ? user.getVerificationToken().getValue() : null;
        if (token == null) {
            return;
        }
        String userName = user.getAccountData().getFirstName();
        String email = user.getAccountData().getEmail();
        //todo: temporary solution, implement the valid link
        String link = "http://localhost:8761/auth/registrationConfirm?token=" + token;
        messengerRestDelegate.sendConfirmationLink(userName, email, link, requestContext.getRequest().getLocalName());
    }
}
