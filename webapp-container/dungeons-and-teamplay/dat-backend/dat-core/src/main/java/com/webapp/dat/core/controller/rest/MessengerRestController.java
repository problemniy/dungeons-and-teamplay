package com.webapp.dat.core.controller.rest;

import com.webapp.dat.core.component.service.IMsgBusinessManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/msg")
public class MessengerRestController {

    private final IMsgBusinessManagementService businessManagementService;

    @Autowired
    public MessengerRestController(IMsgBusinessManagementService businessManagementService) {
        this.businessManagementService = businessManagementService;
    }
}
