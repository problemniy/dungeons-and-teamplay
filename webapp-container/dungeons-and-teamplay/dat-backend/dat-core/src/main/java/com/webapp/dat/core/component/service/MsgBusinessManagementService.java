package com.webapp.dat.core.component.service;

import com.tools.wbe.core.component.service.WebBusinessEventsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MsgBusinessManagementService implements IMsgBusinessManagementService {

    private final WebBusinessEventsService webBusinessEventsService;

    @Autowired
    public MsgBusinessManagementService(WebBusinessEventsService webBusinessEventsService) {
        this.webBusinessEventsService = webBusinessEventsService;
    }
}
