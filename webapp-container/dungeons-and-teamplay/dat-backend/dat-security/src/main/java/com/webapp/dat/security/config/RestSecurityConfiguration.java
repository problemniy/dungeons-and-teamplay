package com.webapp.dat.security.config;

import com.webapp.dat.gateway.component.delegate.AuthenticationRestDelegate;
import com.webapp.dat.gateway.component.delegate.IAuthenticationRestDelegate;
import com.webapp.dat.gateway.component.delegate.IMessengerRestDelegate;
import com.webapp.dat.gateway.component.delegate.MessengerRestDelegate;
import com.webapp.dat.gateway.component.provider.MicroServiceProvider;
import com.webapp.dat.gateway.component.service.IRestSecurityService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestSecurityConfiguration {

    private final RestTemplate restTemplate;
    private final MicroServiceProvider microServiceProvider;
    private final IRestSecurityService restSecurityService;

    public RestSecurityConfiguration(RestTemplate restTemplate, MicroServiceProvider microServiceProvider, @Qualifier("authenticationRestSecurityService") IRestSecurityService restSecurityService) {
        this.restTemplate = restTemplate;
        this.microServiceProvider = microServiceProvider;
        this.restSecurityService = restSecurityService;
    }

    @Bean("securedAuthenticationRestDelegate")
    public IAuthenticationRestDelegate authenticationRestDelegate() {
        return new AuthenticationRestDelegate(restTemplate, microServiceProvider, restSecurityService);
    }

    @Bean("securedMessengerRestDelegate")
    public IMessengerRestDelegate messengerRestDelegate() {
        return new MessengerRestDelegate(restTemplate, microServiceProvider, restSecurityService);
    }
}
