package com.webapp.dat.security.component.service;

import com.webapp.dat.data.model.player.DefaultUser;
import com.webapp.dat.data.model.player.User;
import com.webapp.dat.gateway.component.delegate.IAuthenticationRestDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service("authenticationUserDetailsService")
public class AuthenticationUserDetailsService implements IAuthenticationUserDetailsService {

    private final IAuthenticationRestDelegate authenticationRestDelegate;

    private final Collection<DefaultUser> springDefaultUsers;

    @Autowired
    public AuthenticationUserDetailsService(@Qualifier("securedAuthenticationRestDelegate") IAuthenticationRestDelegate authenticationRestDelegate,
                                            @Qualifier("springDefaultUsers") Collection<DefaultUser> springDefaultUsers) {
        this.authenticationRestDelegate = authenticationRestDelegate;
        this.springDefaultUsers = springDefaultUsers;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDetails userDetails = checkDefaultUser(username);
        if (userDetails == null) {
            ResponseEntity<User> response = authenticationRestDelegate.getUserByName(username);
            userDetails = response != null ? response.getBody() : null;
            if (userDetails == null) {
                throw new UsernameNotFoundException("User was not found!");
            }
        }
        return userDetails;
    }

    private UserDetails checkDefaultUser(String username) {
        return springDefaultUsers.stream().filter((defaultUser) -> defaultUser.getUsername().equals(username)).findAny().orElse(null);
    }
}
