package com.webapp.dat.security.component.service;

import org.springframework.security.core.userdetails.UserDetailsService;

public interface IAuthenticationUserDetailsService extends UserDetailsService {

}
