package com.webapp.dat.security.handler;

import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.ExceptionMappingAuthenticationFailureHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class CustomAuthenticationFailureHandler extends ExceptionMappingAuthenticationFailureHandler {

    public CustomAuthenticationFailureHandler(String failureUrl) {
        setDefaultFailureUrl(failureUrl);
        setRedirectStrategy(new DefaultRedirectStrategy() {
            @Override
            public void sendRedirect(HttpServletRequest request, HttpServletResponse response, String url) throws IOException {
                // prevent redirection and just set unauthorized exception because of single page application
                Exception exception = (Exception) request.getSession().getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
                if (isUseForward()) {
                    exception = (Exception) request.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
                } else {
                    HttpSession session = request.getSession(false);
                    if (session != null || isAllowSessionCreation()) {
                        exception = (Exception) request.getSession().getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
                    }
                }
                // todo: make localized exception message
                if (exception instanceof DisabledException) {
                    response.sendError(HttpStatus.NOT_ACCEPTABLE.value(), exception.getMessage());
                    return;
                }
                response.sendError(HttpStatus.UNAUTHORIZED.value(), exception != null ? exception.getMessage() : "Error: Unauthorized");
            }
        });
    }
}
