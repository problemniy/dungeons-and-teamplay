package com.webapp.dat.security.provider;

import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

public class CustomDaoAuthenticationProvider extends DaoAuthenticationProvider {

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        try {
            return super.authenticate(authentication);
        }
        // because we need to check invalid credentials for user before disable exception
        catch (DisabledException disableException) {
            String userName = authentication.getName();
            UserDetails user = getUserCache().getUserFromCache(userName);
            if (user == null) {
                user = retrieveUser(userName, (UsernamePasswordAuthenticationToken) authentication);
            }
            additionalAuthenticationChecks(user, (UsernamePasswordAuthenticationToken) authentication);
            throw disableException;
        }
    }
}
