package com.webapp.dat.security.utils;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.http.HttpHeaders;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class RestAuthenticationHelper {

    private static final String X_CSRF_TOKEN_KEY = "x-xsrf-token";
    private static final String X_CSRF_TOKEN_VALUE_KEY = "XSRF-TOKEN=";
    private static final String COOKIE_KEY = "Cookie";

    private RestAuthenticationHelper() {
        throw new IllegalStateException("Utility class");
    }

    public static HttpHeaders createSecuredHeaders(String username, String password) {
        return new HttpHeaders() {{
            String authentication = username + ":" + password;
            byte[] encodedAuth = Base64.encodeBase64(
                    authentication.getBytes(StandardCharsets.US_ASCII));
            String authHeader = "Basic " + new String(encodedAuth);
            set("Authorization", authHeader);
        }};
    }

    public static HttpHeaders createCsrfSecuredHeaders(String username, String password, String csrfToken) {
        HttpHeaders headers = createSecuredHeaders(username, password);
        headers.add(X_CSRF_TOKEN_KEY, csrfToken);
        headers.add(COOKIE_KEY, X_CSRF_TOKEN_VALUE_KEY + csrfToken);
        return headers;
    }

    public static HttpHeaders clearCsrfTokens(HttpHeaders headers) {
        HttpHeaders newHeaders = new HttpHeaders(headers);
        newHeaders.remove(X_CSRF_TOKEN_KEY);
        List<String> actualValues = newHeaders.get(COOKIE_KEY);
        if (actualValues != null && !actualValues.isEmpty()) {
            List<String> copiedValues = new ArrayList<>(actualValues).stream().filter(val -> !val.startsWith(X_CSRF_TOKEN_VALUE_KEY)).collect(Collectors.toList());
            if (copiedValues.size() != actualValues.size()) {
                newHeaders.remove(COOKIE_KEY);
                copiedValues.forEach(val -> {
                    newHeaders.add(COOKIE_KEY, val);
                });
            }
        }
        return newHeaders;
    }
}
