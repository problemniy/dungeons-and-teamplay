package com.webapp.dat.security.service;

import com.webapp.dat.data.common.utils.AuthorityDataUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.List;
import java.util.Map;

public class UrlAuthorizationService {

    private final Map<String, List<String>> authorityUrlMatchers;

    public UrlAuthorizationService(Map<String, List<String>> authorityUrlMatchers) {
        this.authorityUrlMatchers = authorityUrlMatchers;
    }

    public boolean isAccessible(String url, Authentication authentication) {
        if (url == null) {
            return true;
        }
        for (Map.Entry<String, List<String>> entry : authorityUrlMatchers.entrySet()) {
            for (String matcher : entry.getValue()) {
                String checkStr = getMathcerToCheck(matcher);
                if (url.contains(checkStr)) {
                    for (GrantedAuthority authority : authentication.getAuthorities()) {
                        if (entry.getKey().equals(AuthorityDataUtils.asRole(authority.getAuthority()))) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    private String getMathcerToCheck(String matcher) {
        String[] keys = matcher.split("/");
        StringBuilder resultBuilder = new StringBuilder();
        for (int i = 0; i < keys.length; i++) {
            if (keys[i].equals("**")) {
                // remove last separate
                resultBuilder.deleteCharAt(resultBuilder.toString().toCharArray().length - 1);
                break;
            }
            resultBuilder.append(keys[i]);
            if (i == 0 && keys[i].equals("") || i < keys.length - 1) {
                resultBuilder.append("/");
            }
        }
        return resultBuilder.toString();
    }
}
