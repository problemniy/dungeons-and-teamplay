package com.webapp.dat.security.component.service;

import com.webapp.dat.data.model.player.DefaultUser;
import com.webapp.dat.gateway.component.service.IRestSecurityService;
import com.webapp.dat.security.utils.RestAuthenticationHelper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletRequest;

@Service("authenticationRestSecurityService")
public class RestSecurityService implements IRestSecurityService {

    private final DefaultUser restTemplateUser;

    public RestSecurityService(@Qualifier("restTemplateUser") DefaultUser restTemplateUser) {
        this.restTemplateUser = restTemplateUser;
    }

    @Override
    public HttpHeaders getSecuredHeaders() {
        return RestAuthenticationHelper.createSecuredHeaders(restTemplateUser.getUsername(), restTemplateUser.getServicePassword());
    }

    @Override
    public HttpHeaders getCsrfSecuredHeaders(HttpServletRequest request) {
        CsrfToken csrf = (CsrfToken) request.getAttribute(CsrfToken.class.getName());
        Assert.notNull(csrf, "Http request should have csrf token attribute");
        return RestAuthenticationHelper.createCsrfSecuredHeaders(restTemplateUser.getUsername(), restTemplateUser.getServicePassword(), csrf.getToken());
    }

    @Override
    public HttpHeaders clearCsrfTokens(HttpHeaders headers) {
        return RestAuthenticationHelper.clearCsrfTokens(headers);
    }
}
