package com.webapp.dat.security.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.lang.Nullable;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

public class TokenAuthenticationHelper {

    //todo: expiration time should be saved in configuration file
    private static final long EXPIRATION_TIME = 1000 * 60 * 60; // 60 minutes

    private static final String COOKIE_AUTH_KEY = "DAT-BEARER";
    private static final String COOKIE_PATH = "/";
    private static final String SECRET_KEY = "DAT-PROJECT-SECRET";
    private static final String CLAIM_NAME = "authorities";

    private TokenAuthenticationHelper() {
        throw new IllegalStateException("Utility class");
    }

    public static void addAuthentication(HttpServletResponse response, Authentication authentication) {
        String jwt = generateJwt(authentication.getName(), authentication.getAuthorities());
        Cookie cookie = new Cookie(COOKIE_AUTH_KEY, jwt);
        cookie.setHttpOnly(true);
        cookie.setPath(COOKIE_PATH);
        cookie.setMaxAge((int) (EXPIRATION_TIME * 60));
        response.addCookie(cookie);
    }

    public static void deleteAuthentication(HttpServletRequest request, HttpServletResponse response) {
        Cookie cookie = WebUtils.getCookie(request, COOKIE_AUTH_KEY);
        if (cookie != null) {
            cookie.setPath(COOKIE_PATH);
            cookie.setValue(null);
            cookie.setMaxAge(0);
            response.addCookie(cookie);
        }
    }

    private static String generateJwt(String userName, Collection<? extends GrantedAuthority> authorities) {
        String authoritiesStr = authorities.stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.joining(","));
        return Jwts.builder()
                .setSubject(userName)
                .claim(CLAIM_NAME, authoritiesStr)
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, SECRET_KEY)
                .compact();
    }

    @Nullable
    public static Authentication getAuthentication(HttpServletRequest request, HttpServletResponse response) {
        Cookie cookie = WebUtils.getCookie(request, COOKIE_AUTH_KEY);
        String token = cookie != null ? cookie.getValue() : null;
        if (token != null) {
            try {
                Claims claims = Jwts.parser()
                        .setSigningKey(SECRET_KEY)
                        .parseClaimsJws(token)
                        .getBody();

                Collection<? extends GrantedAuthority> authorities =
                        Arrays.stream(claims.get(CLAIM_NAME).toString().split(","))
                                .map(SimpleGrantedAuthority::new)
                                .collect(Collectors.toList());

                String userName = claims.getSubject();
                Authentication authentication = null;
                if (userName != null) {
                    // refresh token
                    authentication = new UsernamePasswordAuthenticationToken(userName, null, authorities);
                    String jwt = generateJwt(authentication.getName(), authentication.getAuthorities());
                    updateAccessToken(request, response, jwt);
                }
                return authentication;
            } catch (ExpiredJwtException ex) {
                // reset authentication
                deleteAuthentication(request, response);
                return null;
            }
        }
        return null;
    }

    private static void updateAccessToken(HttpServletRequest request, HttpServletResponse response, String jwt) {
        Cookie cookie = WebUtils.getCookie(request, COOKIE_AUTH_KEY);
        if (cookie != null) {
            cookie.setPath(COOKIE_PATH);
            cookie.setValue(jwt);
            cookie.setMaxAge((int) (EXPIRATION_TIME * 60));
            response.addCookie(cookie);
        }
    }
}
