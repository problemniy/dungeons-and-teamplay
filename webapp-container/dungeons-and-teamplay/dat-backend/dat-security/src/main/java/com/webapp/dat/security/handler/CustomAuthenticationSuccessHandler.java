package com.webapp.dat.security.handler;

import com.webapp.dat.security.service.UrlAuthorizationService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CustomAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    private RequestCache requestCache = new HttpSessionRequestCache();

    private final UrlAuthorizationService urlAuthorizationService;

    public CustomAuthenticationSuccessHandler(UrlAuthorizationService urlAuthorizationService) {
        this.urlAuthorizationService = urlAuthorizationService;
        setRequestCache(requestCache);
        setRedirectStrategy(new DefaultRedirectStrategy() {
            @Override
            public void sendRedirect(HttpServletRequest request, HttpServletResponse response, String url) throws IOException {
                // prevent redirect and just set referer header because of single page application
                response.setHeader(HttpHeaders.REFERER, url);
            }
        });
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws ServletException, IOException {
        checkAuthoritiesOnSuccess(request, response, authentication);
        super.onAuthenticationSuccess(request, response, authentication);
    }

    private void checkAuthoritiesOnSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
        SavedRequest savedRequest = requestCache.getRequest(request, response);
        if (savedRequest != null && !urlAuthorizationService.isAccessible(savedRequest.getRedirectUrl(), authentication)) {
            response.sendError(HttpStatus.UNAUTHORIZED.value(), "Bad credentials");
        }
    }
}
