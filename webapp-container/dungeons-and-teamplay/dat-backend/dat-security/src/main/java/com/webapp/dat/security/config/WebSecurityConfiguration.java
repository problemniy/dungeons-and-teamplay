package com.webapp.dat.security.config;

import com.webapp.dat.data.constants.AuthorityConstants;
import com.webapp.dat.security.filter.JWTAuthenticationFilter;
import com.webapp.dat.security.filter.JWTLoginFilter;
import com.webapp.dat.security.handler.CustomAuthenticationFailureHandler;
import com.webapp.dat.security.handler.CustomAuthenticationSuccessHandler;
import com.webapp.dat.security.handler.RedirectAccessDeniedHandler;
import com.webapp.dat.security.provider.CustomDaoAuthenticationProvider;
import com.webapp.dat.security.service.UrlAuthorizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.provisioning.UserDetailsManagerConfigurer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    // todo: create specific class for url constants
    private static final String DEFAULT_URL = "/game";
    private static final String LOGIN_PROCESS_URL = "/auth/sign";
    private static final String LOGIN_SERVICE_URL = "/sauth/sign";

    private final AccessUsersConfiguration accessUsersConfiguration;
    private final UserDetailsService authenticationUserDetailsService;
    private final PasswordEncoder passwordEncoder;

    private static final String PERMIT_ALL = "PERMIT_ALL";
    private static final Map<String, List<String>> authorityUrlMatchers = new HashMap<>() {{
        put(PERMIT_ALL, Arrays.asList("/eureka/css/**", "/eureka/images/**", "/eureka/fonts/**", "/eureka/js/**", "/auth/**"));
        put(AuthorityConstants.AUTH_SYSTEM, Collections.singletonList("/eureka/**"));
        put(AuthorityConstants.AUTH_ADMIN, Arrays.asList("/edashboard", "/wbe/**"));
        put(AuthorityConstants.AUTH_USER, Collections.singletonList("/user/**"));
    }};

    @Autowired
    public WebSecurityConfiguration(AccessUsersConfiguration accessUsersConfiguration, UserDetailsService authenticationUserDetailsService, PasswordEncoder passwordEncoder) {
        this.accessUsersConfiguration = accessUsersConfiguration;
        this.authenticationUserDetailsService = authenticationUserDetailsService;
        this.passwordEncoder = passwordEncoder;
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider = new CustomDaoAuthenticationProvider();
        authProvider.setUserDetailsService(authenticationUserDetailsService);
        authProvider.setPasswordEncoder(passwordEncoder);
        return authProvider;
    }

    @Bean
    public UrlAuthorizationService urlAuthorizationService() {
        return new UrlAuthorizationService(authorityUrlMatchers);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        for (AccessUsersConfiguration.ConfigUser user : accessUsersConfiguration.getDefaultUsers()) {
            String[] roles = user.getRoles().toArray(String[]::new);
            String[] authorities = user.getAuthorities().toArray(String[]::new);
            UserDetailsManagerConfigurer.UserDetailsBuilder userBuilder = auth.inMemoryAuthentication().passwordEncoder(new PasswordEncoder() {
                @Override
                public String encode(CharSequence rawPassword) {
                    // because users already have encoded passwords
                    return rawPassword.toString();
                }

                @Override
                public boolean matches(CharSequence rawPassword, String encodedPassword) {
                    return rawPassword.toString().equals(encodedPassword);
                }
            })
                    .withUser(user.getName()).password(user.getPassword());
            if (roles.length > 0) {
                userBuilder.roles(roles);
            }
            if (authorities.length > 0) {
                userBuilder.authorities(authorities);
            }
        }
        auth.authenticationProvider(authenticationProvider());
    }

    @Override
    public void configure(WebSecurity web) {
        // uncomment to enable spring security debugging
        // web.debug(true);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        registerUrlAccess(http);
        http
                .formLogin()
                .and()
                .logout().permitAll().and().httpBasic()
                .and()
                .csrf().ignoringAntMatchers("/eureka/**")
                .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
                .and().formLogin()
                .loginPage(LOGIN_SERVICE_URL)
                .loginProcessingUrl(LOGIN_PROCESS_URL)
                .defaultSuccessUrl(DEFAULT_URL, true)
                .successHandler(new CustomAuthenticationSuccessHandler(urlAuthorizationService()))
                .failureHandler(new CustomAuthenticationFailureHandler(DEFAULT_URL))
                .and()
                .addFilterBefore(new JWTLoginFilter(LOGIN_PROCESS_URL, authenticationManager(), urlAuthorizationService()), UsernamePasswordAuthenticationFilter.class)
                .addFilterBefore(new JWTAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class)
                .exceptionHandling()
                .accessDeniedHandler(new RedirectAccessDeniedHandler());
    }

    private void registerUrlAccess(HttpSecurity http) throws Exception {
        ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry urlRegistry = http.authorizeRequests();
        authorityUrlMatchers.forEach((role, matchers) -> {
            String[] arrayRoles = Arrays.copyOf(matchers.toArray(), matchers.toArray().length, String[].class);
            if (PERMIT_ALL.equals(role)) {
                urlRegistry.antMatchers(arrayRoles).permitAll();
            } else {
                urlRegistry.antMatchers(arrayRoles).hasRole(role);
            }
        });
    }
}
